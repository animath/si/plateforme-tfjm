#!/bin/sh

crond -l 0

python manage.py migrate
python manage.py update_index

nginx

if [ "$TFJM_STAGE" = "prod" ]; then
gunicorn -b 0.0.0.0:8000 \
        --workers=2 \
        --threads=4 \
        --worker-class=uvicorn.workers.UvicornWorker \
        tfjm.asgi \
        --access-logfile '-' \
        --error-logfile '-'
else
gunicorn -b 0.0.0.0:8000 \
        --workers=2 \
        --threads=4 \
        --worker-class=uvicorn.workers.UvicornWorker \
        tfjm.asgi \
        --access-logfile '-' \
        --error-logfile '-' \
        --reload
fi
