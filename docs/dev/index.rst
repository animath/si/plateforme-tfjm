Développer la plateforme
========================

Cette page est dédiée aux responsables informatiques qui cherchent à contribuer à la plateforme.

Présentation
------------

La plateforme d'inscription du TFJM² actuelle est née lors de l'édition 2020. Elle n'est
pas la première à exister, elle succède à une précédente, moins fonctionnelle, dont les
sources ont été perdues. Elle a été développée par Emmy D'Anello, bénévole pour Animath,
qui la maintient au moins jusqu'en 2024.

La plateforme est développée en Python, utilisant le framework web
`Django <https://www.djangoproject.com/>`_. Elle est diponible librement sous licence GPLv3
à l'adresse `<https://gitlab.com/animath/si/plateforme-tfjm>`_.

L'instance de production est accessible à l'adresse `<https://inscription.tfjm.org/>`_.
Une instance de développement est accessible à l'adresse `<https://inscription-dev.tfjm.org/>`_.

Les deux instances sont hébergées sur le serveur d'Animath. La documentation spécifique
à l'installation des services d'Animath peut être trouvée à l'adresse
`<https://doc.animath.live/>`_.
