Transition d'années
===================

Entre deux sessions du TFJM², certaines opérations doivent être effectuées chaque année,
afin de réinitialiser les données et de passer à l'année suivante.

Réinitialisation de la base de données
--------------------------------------

Conservation des autorisations de droit à l'image
"""""""""""""""""""""""""""""""""""""""""""""""""

La base de données du TFJM² est supprimée chaque année, avant chaque tournoi. Il n'y a
pas de conservation de données personnelles à l'exception des autorisations de droit
à l'image qui doivent être conservées pour des raisons légales pendant 5 ans.

Elles doivent alors être stockées sur Owncloud. Pour cela, il faut commencer par créer
un dossier dans Owncloud, qui stockera lesdites autorisations.

Rendez-vous ensuite dans le conteneur Docker et exécuter le script :

.. code:: bash

   ./manage.py export_photo_authorizations

Cela a pour effet de générer un dossier dans ``output/photo_authorizations``, qui contient
un dossier par équipe avec les différentes autorisations de droit à l'image.

Il faut maintenant récupérer ce dossier. Sortir du conteneur, et exécuter dans ``/srv/TFJM`` :

.. code:: bash

   sudo docker cp tfjm-inscription-1:/code/output/photo_authorizations .
   sudo mv photo_authorizations/* "data/owncloud/data/Emmy/files/Autorisations de droit à l'image/Autorisations de droit à l'image 2024/"
   sudo chown -R www-data:root "data/owncloud/data/Emmy/files/Autorisations de droit à l'image/Autorisations de droit à l'image 2024"
   sudo rmdir photo_authorizations

Il faut enfin réactualiser Owncloud. Exécuter en tant que www-data :

.. code:: bash

   sudo docker compose exec -u www-data cloud php occ files:scan Emmy

Vérifiez enfin que les fichiers sont bien accessibles dans l'interface Web.
Ne pas oublier enfin de partager le dossier.


Sauvegarde de secours
"""""""""""""""""""""

Si les données doivent être supprimées, il peut être utile de réaliser une sauvegarde à conserver
quelques mois.

.. danger::

   Cette sauvegarde ne doit être faite qu'à des fins utiles et supprimée dès que plus nécessaire.

Sauvegardez alors le dossier ``/srv/TFJM/data/inscription/media`` et exportez la base de données :

.. code:: bash

   sudo cp -r data/inscription/media data/inscription/media-2024
   sudo docker compose exec -u postgres postgres pg_dump inscription_tfjm | sudo tee inscription_tfjm_bkp_2024.sql > /dev/null


Réinitialisation effective
""""""""""""""""""""""""""

Il est désormais possible de réinitialiser la base de données, après avoir éteint le serveur :

.. code:: bash

   sudo docker compose stop inscription
   sudo rm -r data/inscription/media/*
   sudo docker compose exec -u postgres postgres dropdb inscription_tfjm
   sudo docker compose exec -u postgres postgres createdb -O inscription_tfjm inscription_tfjm

Redémarrez enfin le serveur (les migrations seront créées automatiquement)
et créez un nouveau compte administrateur⋅rice :

.. code:: bash

   sudo docker compose up -d inscription
   sudo docker compose exec inscription bash
   ./manage.py createsuperuser

Vérifiez finalement le bon fonctionnement du site.


Sites Django
""""""""""""

Après avoir réinitialisé les données, il faut mettre à jour le site Django, qui permettra
d'avoir notamment des noms de domaine correct dans les mails envoyés.

Se connecter alors sur le site réouvert, puis dans la partie « Administration », chercher la
section « Sites » et modifier l'unique site présent. Vous pouvez ensuite effectuer les modifications
à réaliser.


Nouveaux paramètres pour la nouvelle année
------------------------------------------

Certains paramètres doivent être modifiés pour prendre en compte la nouvelle année.

Dates d'inscription
"""""""""""""""""""

Les inscriptions sont permises uniquement entre l'ouverture et la fermeture, afin d'éviter
d'avoir des personnes s'inscrivant en dehors du TFJM².

Pour cela, dans votre projet local, rendez-vous dans ``tfjm/settings.py`` et cherchez
le paramètre ``REGISTRATION_DATES`` (pour le TFJM²). Modifiez alors les sous-paramètres
``open`` et ``close`` pour définir les dates pendant lesquelles les inscriptions des
participant⋅es sont permises pour cette nouvelle année. Elles doivent être au format ISO.

Exemple pour l'année 2025 où les inscriptions ouvrent au 8 janvier midi pour fermer
le 2 mars à 22h :

.. code:: python

    REGISTRATION_DATES = dict(
        open=datetime.fromisoformat("2025-01-15T12:00:00+0100"),
        close=datetime.fromisoformat("2025-03-02T22:00:00+0100"),
    )

Il faudra ensuite commiter la modification et redémarrer le serveur pour que la modification
prenne effet.


Noms des problèmes
""""""""""""""""""

Toujours dans la configuration dans ``tfjm/settings.py``, la liste des problèmes doit être
modifiée pour que leurs noms s'affichent correctement lors du tirage au sort.

Cherchez le paramètre ``PROBLEMS`` et mettez alors à jour la liste, dans l'ordre, des noms
des problèmes.

À nouveau, il est nécessaire de commiter la modification et redémarrer le serveur.


Paramètres des tournois
"""""""""""""""""""""""

Il faut enfin paramétrer les différentes dates des tournois.

Pour cela, connectez-vous sur la plateforme (avec un compte administrateur⋅rice), et dans l'onglet
« Tournois », vous pouvez créer les différents tournois avec les différentes dates pour chaque tournoi.
Plus d'information sur les différents paramètres dans la `section concernée
<../orga.html#creer-un-tournoi>`_


À la fin du tournoi
-------------------

Lorsque le tournoi est terminé, il faut récupérer les informations à stocker de façon pérenne,
notamment les solutions des équipes, les résultats ainsi que les autorisation de droit à l'image
comme indiqué précédemment.

Conservation des autorisations de droit à l'image
"""""""""""""""""""""""""""""""""""""""""""""""""

Se référer à la section plus haut.


Conservation des solutions des équipes
""""""""""""""""""""""""""""""""""""""

Le processus est très similaire à la conservation des autorisations de droit à l'image.
Il faut d'abord, dans le conteneur, lancer le script dédié pour récupérer les solutions
dans ``/code/output/solutions`` :

.. code:: bash

   ./manage.py export_solutions

On sort du conteneur et on récupère les solutions pour les déplacer dans Owncloud :

.. code:: bash

   sudo docker cp tfjm-inscription-1:/code/output/solutions .
   sudo mv solutions/* "data/owncloud/data/Emmy/files/Solutions écrites 2024/"
   sudo chown -R www-data:root "data/owncloud/data/Emmy/files/Solutions écrites 2024"
   sudo rmdir solutions

Il faut enfin réactualiser Owncloud. Exécuter en tant que www-data :

.. code:: bash

   sudo docker compose exec -u www-data cloud php occ files:scan Emmy

Vérifiez enfin que les fichiers sont bien accessibles dans l'interface Web.
Ne pas oublier enfin de partager le dossier.


Génération de la page de résultats Wordpress
""""""""""""""""""""""""""""""""""""""""""""

Pour finir, il est possible de récupérer les notes pour chaque tournoi afin de générer
la page Wordpress dans la section *Éditions précédentes*.

Il suffit de lancer le script ``./manage.py export_results``, qui donne le texte brut pour
Wordpress à ajouter sur la page de l'édition qui vient de se terminer dans l'onglet
*Éditions précédentes*.

Pensez à bien inclure sur cette page le lien vers les problèmes de l'année, ainsi que le
lien vers le dossier partagé dans le Owncloud concernant les solutions des équipes.

Assurez-vous de mettre à jour la page *Éditions précédentes* afin d'inclure le lien vers
la page nouvellement créée.
