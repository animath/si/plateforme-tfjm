Installation de la plateforme du TFJM²
======================================

Cette page documente la procédure d'installation de la plateforme du TFJM²,
aussi bien dans un environnement de développement que de production.


Installation en production
--------------------------

Installation de Docker
""""""""""""""""""""""

Les outils du TFJM² sont déployés en utilisant `Docker <https://www.docker.com/>`_.
Pour plus de détails sur la configuration pour le TFJM², voir
`la page dédiée à Docker<docker.html>`_.

Commencez par installer Docker et Docker-Compose (qui sont a priori déjà installés
sur la plateforme du TFJM²), en commençant par installer le dépôt APT de Docker :

.. code-block:: bash

    # Add Docker's official GPG key:
    sudo apt update
    sudo apt install ca-certificates curl gnupg
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    sudo chmod a+r /usr/share/keyrings/docker-archive-keyring.gpg

    # Add the repository to Apt sources:
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update

    sudo apt install docker-ce docker-compose-plugin

Installation de la plateforme
"""""""""""""""""""""""""""""

Dans le fichier ``docker-compose.yml``, configurer :

.. code-block:: yaml

    version: "3.8"
    services:
      # […]
      inscription:
        build: https://gitlab.com/animath/si/plateforme.git
        links:
          - postgres
          - redis
          - elasticsearch
        env_file:
          - ./secrets/inscription.env
        restart: always
        volumes:
          - "./data/inscription/media:/code/media"
          - "/etc/localtime:/etc/localtime:ro"
        networks:
          - tfjm
        labels:
          - "traefik.http.routers.inscription-tfjm2.rule=Host(`inscription.tfjm.org`, `inscriptions.tfjm.org`, `plateforme.tfjm.org`)"
          - "traefik.http.routers.inscription-tfjm2.entrypoints=websecure"
          - "traefik.http.routers.inscription-tfjm2.tls.certresolver=mytlschallenge"

      postgres:
        image: postgres:16
        volumes:
          - "./data/postgresql_16:/var/lib/postgresql/data"
        restart: always
        env_file:
          - ./secrets/postgres.env
        networks:
          - tfjm

      redis:
        image: redis:alpine
        restart: always
        networks:
          - tfjm

      elasticsearch:
        image: docker.elastic.co/elasticsearch/elasticsearch:7.17.9
        restart: always
        env_file:
          - ./secrets/elasticsearch.env
        networks:
          - tfjm
      # […]

En cas d'instance de pré-production, il est possible de changer de branche en
rajoutant par exemple ``#dev`` à la fin de l'URL.

Les différents paramètres peuvent être modifiés si nécessaire, à commencer par les
versions des autres services (PostgreSQL, Redis, Elasticsearch). Penser à mettre à jour
cette documentation en cas de mise à jour future. Il est attendu d'avoir un réseau
``tfjm`` configuré :

.. code-block:: yaml

    networks:
    tfjm:
      driver: bridge

L'URL utilisée peut être modifiée en changeant les options de Traefik, section ``labels``.

Configuration
"""""""""""""

La configuration se fait essentiellement dans les fichiers d'environnement.

Pour la base de données, une seule variable d'environnement est requise :
``POSTGRES_PASSWORD``, qui correspond au mot de passe maître de PostgreSQL.

Les variables d'environnement de ElasticSearch se résume à du paramétrage de mémoire :

.. code-block:: bash

    discovery.type=single-node
    ES_JAVA_OPTS=-Xms512m -Xmx512m

Redis n'a pas besoin de configuration particulière.

Enfin, pour la plateforme elle-même, il faut configurer les variables suivantes :

.. code-block:: bash

    TFJM_STAGE=prod
    DJANGO_SECRET_KEY=
    DJANGO_DB_TYPE=PostgreSQL
    DJANGO_DB_HOST=postgres
    DJANGO_DB_PORT=
    DJANGO_DB_NAME=
    DJANGO_DB_USER=
    DJANGO_DB_PASSWORD=
    REDIS_SERVER_HOST=redis
    REDIS_SERVER_PORT=6379
    SMTP_HOST=ssl0.ovh.net
    SMTP_PORT=465
    SMTP_HOST_USER=contact@tfjm.org
    SMTP_HOST_PASSWORD=
    FROM_EMAIL=Contact TFJM²
    SERVER_EMAIL=contact@tfjm.org
    HAYSTACK_INDEX_NAME=inscription-tfjm
    SYMPA_HOST=lists.tfjm.org
    SYMPA_URL=lists.tfjm.org/sympa
    SYMPA_EMAIL=contact@tfjm.org
    SYMPA_PASSWORD=
    HELLOASSO_CLIENT_ID=
    HELLOASSO_CLIENT_SECRET=

* ``TFJM_STAGE`` : ``prod`` ou ``dev``. ``prod`` est utilisé pour la production,
  ``dev`` pour le développement ou pré-production. Permet de désactiver certaines
  fonctionnalités de production, notamment l'envoi de mails, le cache Redis, les listes
  de diffusion, et d'activer le débogage. Les erreurs seront ainsi directement affichées
  en développement tandis qu'elles seront envoyées par mail en production.
* ``DJANGO_SECRET_KEY`` : correspond à
  `la clé secrète Django <https://docs.djangoproject.com/fr/5.0/ref/settings/#secret-key>`_,
  permettant de signer les sessions et les cookies. Il est important de la garder secrète.
  Pour la générer, il est possible d'utiliser la commande
  ``python3 manage.py generate_secret_key``.
* ``DJANGO_DB_TYPE`` : Le type de base de données à utiliser, parmi
  ``PostgreSQL``, ``MySQL`` et ``SQLite``. ``SQLite`` n'est à utiliser qu'en développement
  local.
* ``DJANGO_DB_HOST`` : L'hôte de la base de données pour les bases de données
  ``PostgreSQL`` et ``MySQL``. Pour ``SQLite``, il faut mettre le chemin vers le fichier
  de base de données. Dans l'exemple ci-dessus, ``postgres`` est l'hôte Docker.
* ``DJANGO_DB_PORT`` : Le port de la base de données. (PostgreSQL ou MySQL uniquement)
  Si laissé vide, utilisera le port par défaut du service, ``5432`` pour PostgreSQL et
  ``3306`` pour MySQL.
* ``DJANGO_DB_NAME`` : Le nom de la base de données. (PostgreSQL ou MySQL uniquement)
* ``DJANGO_DB_USER`` : Le nom d'utilisateur de la base de données.
  (PostgreSQL ou MySQL uniquement)
* ``DJANGO_DB_PASSWORD`` : Le mot de passe de la base de données.
  (PostgreSQL ou MySQL uniquement)
* ``REDIS_SERVER_HOST`` : L'hôte de Redis (en production uniquement). Dans l'exemple
  ci-dessus, ``redis`` est l'hôte Docker.
* ``REDIS_SERVER_PORT`` : Le port de Redis (en production uniquement). Si laissé vide,
  utilisera le port par défaut du service, ``6379``.
* ``SMTP_HOST`` : L'hôte du serveur SMTP à utiliser pour envoyer les mails. Utilise par
  défaut le serveur d'OVH.
* ``SMTP_PORT`` : Le port du serveur SMTP à utiliser pour envoyer les mails.
* ``SMTP_HOST_USER`` : Le nom d'utilisateur du serveur SMTP à utiliser pour envoyer les
  mails. Correspond à l'identifiant OVH.
* ``SMTP_HOST_PASSWORD`` : Le mot de passe du serveur SMTP à utiliser pour envoyer les
  mails. Correspond au mot de passe OVH.
* ``FROM_EMAIL`` : Le nom lisible à utiliser comme expéditeur des mails
  (défaut : Contact TFJM²).
* ``SERVER_EMAIL`` : L'adresse mail à utiliser comme expéditeur des mails
  (défaut : contact@tfjm.org).
* ``HAYSTACK_INDEX_NAME`` : Le nom de l'index ElasticSearch à utiliser pour les recherches
  dans ElasticSearch (défaut : inscription-tfjm).
* ``SYMPA_HOST`` : Le domaine des listes de diffusion Sympa utilisé.
* ``SYMPA_URL`` : L'URL du serveur Sympa à utiliser pour gérer les listes de diffusion.
* ``SYMPA_EMAIL`` : L'adresse mail à utiliser pour se connecter à Sympa.
* ``SYMPA_PASSWORD`` : Le mot de passe à utiliser pour se connecter à Sympa.
* ``HELLOASSO_CLIENT_ID`` : L'identifiant client HelloAsso à utiliser pour gérer les
  paiements HelloAsso.
* ``HELLOASSO_CLIENT_SECRET`` : Le secret client HelloAsso à utiliser pour gérer les
  paiements HelloAsso. Doit être maintenu secret.

Installation de la base de données
""""""""""""""""""""""""""""""""""

Pour gérer la base de données PostgreSQL, on peut utiliser les commandes suivantes :

.. code-block:: bash

    sudo docker compose up -d postgres
    sudo docker compose exec -u postgres postgres createuser -P inscription_tfjm  # Création du compte `inscription_tfjm` en demander un mot de passe. À ne faire qu'une seule fois
    sudo docker compose exec -u postgres postgres createdb -O inscription_tfjm inscription_tfjm  # Création de la base de données `inscription_tfjm` en utilisant le compte `inscription_tfjm`
    sudo docker compose exec -u postgres pg_dump inscription_tfjm > inscription_tfjm.sql  # Pour sauvegarder la base de données `inscription_tfjm`
    sudo docker compose exec -u postgres dropdb inscription_tfjm  # Pour supprimer la base de données `inscription_tfjm`

La suppression et la recréation sont utiles en cas de réinitialisation annuelle de la base
de données.


Lancement
"""""""""

Pour lancer la plateforme, il suffit de lancer la commande suivante :

.. code-block:: bash

    sudo docker compose up -d inscription

Pour arrêter la plateforme, il suffit de lancer la commande suivante :

.. code-block:: bash

    sudo docker compose stop inscription

En cas de mise à jour de la plateforme, il suffit de lancer la commande suivante :

.. code-block:: bash

    sudo docker compose up -d --build inscription

Selon le principe de Docker, les données sont conservées dans un volume Docker, ici
dans le dossier ``data/inscription``. Le reste est volatile, puisqu'il peut être recréé
à partir du code source. Attention donc de ne pas coder en production (ce qui est de toute
façon à proscrire !).

Les migrations de base de données sont automatiquement appliquées au lancement de la
plateforme, de même que la collecte de fichiers statiques ou encore la génération de la
documentation. Il n'y a rien à faire de spécial post-lancement, si ce n'est vérifier que
tout fonctionne correctement.


Installation en développement
-----------------------------

L'installation sur une machine locale est plus légère et est utile pour pouvoir tester
rapidement.

Commencez par récupérer le code source de la plateforme :

.. code-block:: bash

    git clone https://gitlab.com/animath/si/plateforme-tfjm.git
    cd plateforme-tfjm

Afin de pouvoir isoler l'environnement de développement, il est conseillé d'utiliser
un environnement virtuel Python. Commencez alors par installer Python (si ce n'est pas
déjà fait, au moins en version 3.10) ainsi que ``virtualenv``, puis vous pouvez créer
l'environnement virtuel, et entrer dedans :

.. code-block:: bash

    python3 -m venv venv
    source venv/bin/activate

Il vous faut ensuite installer les dépendances de la plateforme :

.. code-block:: bash

    pip install -r requirements.txt

Pour exécuter des tests, il est nécessaire d'installer ``tox`` en supplément.

Ensuite, vous devez initialiser la base de données locale (qui sera stockée dans un
fichier SQLite ``db.sqlite3``) :

.. code-block:: bash

    python3 manage.py migrate

Enfin, vous pouvez lancer la plateforme :

.. code-block:: bash

    python3 manage.py runserver

Vous pouvez alors accéder à la plateforme à l'adresse `<http://localhost:8000/>`_.
Elle se recharge automatiquement à chaque modification du code source, inutile de la
relancer.

Pour arrêter la plateforme, il suffit d'appuyer sur ``Ctrl+C`` dans le terminal.

Pour vous créer un compte administrateur⋅rice, il suffit de lancer la commande suivante :

.. code-block:: bash

    python3 manage.py createsuperuser

En cas de problème, vous pouvez librement supprimer la base de données locale et la
recréer en réexécutant la commande ``python3 manage.py migrate``.
