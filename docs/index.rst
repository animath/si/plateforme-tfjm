Documentation de la plateforme du TFJM²
=======================================

Ce site vise à documenter l'usage de la plateforme de gestion du TFJM², aussi
bien du côté utilisateur⋅rice que du côté organisateur⋅rice ou bien
administrateur⋅rice.


.. toctree::
   :maxdepth: 3
   :caption: Utiliser

   user
   orga
   draw


.. toctree::
   :maxdepth: 3
   :caption: Développer

   dev/index
   dev/install
   dev/transition
