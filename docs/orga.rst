Partie organisateur⋅rices
=========================

.. contents::

Cette page est dédiée aux organisateur⋅rices qui souhaitent utiliser la plateforme pour gérer
les différentes équipes et les inscriptions.


Ajouter un⋅e nouvelleau organisateur⋅rice
-----------------------------------------

Seul⋅es les actuel⋅les organisateur⋅rices peuvent en ajouter de nouvelleaux. Il n'est pas possible
de s'inscrire.

Pour cela, il faut se connecter, aller dans l'onglet « Utilisateur⋅rices », puis « Ajouter un⋅e organisateur⋅rice ».

Les informations suivantes sont demandées :

* Prénom
* Nom de famille
* Adresse e-mail (préférer une adresse institutionnelle)
* Rôle (Bénévole ou administrateur⋅rice)
* Activité professionnelle (permet de savoir d'où viennent les bénévoles)

Les bénévoles peuvent gérer ce qui les concernent (tournois, jurys,…), les administrateur⋅rices ont un accès
intégral à l'ensemble de la plateforme, non restreint. Ce dernier statut ne devrait être réservé qu'aux membres du CNO.

Une fois le formulaire validé, un mail est envoyé permettant de définir son mot de passe. Iel peut ensuite se
connecter en utilisant son adresse e-mail et son mot de passe.


Gestion des tournois
--------------------

Créer un tournoi
""""""""""""""""

.. important::
   Seul⋅es les administrateur⋅rices peuvent créer des tournois.

Pour créer un tournoi, il suffit de cliquer dans l'onglet « Tournois » puis « Ajouter un tournoi ».
Les descriptions des différents paramètres sont dans la section suivante.


Modifier un tournoi
"""""""""""""""""""

.. important::
   Seul⋅es les administrateur⋅rices ainsi que les organisateur⋅rices dudit tournoi peuvent modifier le tournoi.

Pour modifier un tournoi, il faut déjà se rendre sur la page du tournoi : onglet « Tournois » puis cliquer sur
le bon tournoi. Le bouton « Modifier le tournoi » devrait être accessible.

.. warning::
   Si le bouton n'est pas visible, vérifiez que vous êtes bien connecté⋅e, et que vous êtes bien marqué⋅es parmi
   les organisateur⋅rices. N'hésitez pas à les contacter si ce n'est pas le cas.

Les informations suivantes peuvent être modifiées :

* Nom du tournoi
* Date de début (le samedi)
* Date de fin (le dimanche)
* Adresse du lieu physique
* Nombre indicatif maximal d'équipes autorisées
  (un multiple de 3, n'est là qu'à titre indicatif et n'est pas bloquant pour la suite)
* Prix demandé aux participant⋅es, normalement 21 € sauf pour la finale 35 € (hors boursièr⋅es et tournois en visio)
* La case « À distance » doit rester décochée tant que les tournois en visio n'ont pas repris
* Date limite d'inscription : date jusqu'à laquelle les équipes peuvent finaliser leur inscription (non bloquant).
  En général le mois précédent le tournoi
* Date limite pour envoyer les solutions : date jusqu'à laquelle les équipes peuvent soumettre leurs solutions
  (au-delà, le remplacement de solutions déjà soumises n'est plus permis mais l'envoi de nouvelles reste possible en
  cas de besoin, à décourager). En général le dimanche avant le tournoi vers 22h
* Tirage au sort : date indicative qui dit quand le tirage au sort va se dérouler. En général le mardi précédent
  le tournoi vers 20h
* Date limite pour envoyer les notes de synthèses pour la première/seconde phase : même règle que pour les solutions,
  mais pour les notes de synthèse. Généralement le vendredi à 22h pour le premier tour et le dimanche à 10h pour le
  second tour
* Date à laquelle les solutions pour le second tour sont accessibles : seules les solutions pour le premier tour sont
  directement accessible après le tirage au sort, celles pour le second tour sont libérées automatiquement une fois
  cette date passée. Généralement le samedi entre 17h et 18h (à adapter)
* Description
* Organisateur⋅rices : liste des personnes qui organisent le tournoi et peuvent le gérer numériquement. N'inclut pas
  les juré⋅es
* Finale (admin uniquement) : cette case ne doit être cochée que pour le tournoi de la finale.


Liste des équipes
"""""""""""""""""

Lorsque les équipes choisissent leur tournoi, elle est répertoriée sur la page du tournoi.


Valider une équipe
""""""""""""""""""

Lorsqu'une équipe a finalisé son inscription et a demandé à être validée, un mail est envoyé à l'ensemble des
organisateur⋅rices. Sur l'interface du tournoi, il est possible de cliquer sur l'équipe et accéder à l'ensemble
de ses informations :

* Nom de l'équipe
* Trigramme
* Encadrant⋅es
* Participant⋅es
* Diverses autorisations
* Lettre de motivation

Lorsqu'il est temps de valider l'équipe, un formulaire dédié apparaît en bas. Un texte peut être envoyé à l'équipe,
et le choix est proposé de valider l'équipe ou non.


.. TODO
.. note::
   Cette documentation sera complétée à l'avenir pour prendre en compte les enjeux du tournoi au moment venu.
