Tirage au sort
==============

La phase de tirage au sort est celle qui va déterminer d'une part
dans quelle poule se trouve chaque équipe et dans quelle ordre elles
défendront leurs problèmes, et d'autre part quel problème défendra
quelle équipe. Cette phase a lieu dans la semaine qui précède le
tournoi, typiquement le mardi entre 19h et 21h.

Exception pour la finale nationale : seul le premier tour est tiré
au sort à ce moment-là, le tirage au sort pour le second tour est
réalisé immédiatement après le premier tour et dépend des résultats.
Sinon, pour les finales régionales, les tirages au sort pour les
tours 1 et 2 sont réalisés successivement, bien que les solutions
à opposer et à rapporter pour le second tour ne sont pas accessibles
avant la fin du premier tour.

**Disclaimer :** si cette documentation est normalement tenue à jour,
seul le règlement et ses annexes font foi. Elle est essentiellement ici
pour décrire le fonctionnement de la plateforme vis-à-vis du tirage
au sort. En cas de litige, merci de se fier au règlement, et de
s'y référer `sur le site du TFJM² <https://tfjm.org/reglement/>`_.


Principe
--------

.. warning:: Cette section arborde en détail le fonctionnement théorique
   du tirage au sort. Si vous souhaitez comprendre comment se déroule
   le tirage au sort en pratique sur la plateforme, merci de vous référer
   directement à la section `Déroulement du tirage`_.

Composition des poules
~~~~~~~~~~~~~~~~~~~~~~

Le principe du tirage au sort est détaillé dans la fiche pratique dédiée.

Chaque équipe commence par désigner un⋅e capitaine d'équipe qui s'occupera
de réaliser les tirages et de prendre les décisions.

Les poules sont triées de la plus grande à la plus petite, en terme de
nombre d'équipes.

Les capitaines d'équipe lancent un dé entre 1 et 100. S'il y a des égalités,
alors les équipes concernées relancent leur dé jusqu'à ne plus avoir d'égalité.
Ce score détermine les compositions des poules pour les deux tours (sauf pour
la finale nationale ou ce n'est que le premier tour).

On trie ensuite les équipes par ordre croissant de score de dé. On remplit
ensuite les poules une à une de façon gloutonne : si par exemple la
première poule est une poule à 3 équipes, alors on prend les 3 premières
équipes de la liste et on les place dans cette poule. On recommence ensuite
avec la deuxième poule, etc.

Au sein d'une poule, l'ordre de passage des équipes est déterminé par
l'ordre des restes modulo 100 des scores de dé multipliés par 27.
Par exemple, si les scores de dé sont 12, 34 et 56, alors si on
multiplie par 27 et qu'on prend le reste modulo 100, on obtient
respectivement 24, 82 et 52. L'ordre de passage sera donc l'équipe 1,
l'équipe 3, puis l'équipe 2. Ce choix est réalisé pour avoir un semblant
déterministe de mélange.

Pour le second tour, on considère à nouveau les scores de dé, où l'équipe
qui a eu le score le plus faible sera dans la première poule, celle qui
a eu le second score le plus faible dans la deuxième poule, etc. L'ordre
de passage est cette fois-ci plus simple, puisqu'il est directement croissant
avec les scores de dé. Exception : pour les poules à 5, l'équipe avec le
score le plus gros sera la dernière de la première poule, et il ne peut
y avoir qu'une seule poule à 5 équipes.

Considérons par exemple un tournoi fictif composé de 11 équipes, avec
une poule de 5 équipes et deux poules de 3 équipes. Les équipes sont
AAA, BBB, CCC, DDD, EEE, FFF, GGG, HHH, III, JJJ et KKK. Les scores
de dés sont :

.. table:: Exemple de tirage de dés et de répartition dans les poules

   +-----------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Équipe                      | AAA | BBB | CCC | DDD | EEE | FFF | GGG | HHH | III | JJJ | KKK |
   +=============================+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+
   | Score de dé                 |  45 |  17 |  64 |  3  |  98 |  41 |  34 |  63 |  86 |  23 |  70 |
   +-----------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Poule au 1\ :sup:`er` tour  |  B  |  A  |  B  |  A  |  C  |  A  |  A  |  B  |  C  |  A  |  C  |
   +-----------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Poule au 2\ :sup:`ème` tour |  C  |  B  |  B  |  A  |  A  |  B  |  A  |  A  |  A  |  C  |  C  |
   +-----------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+

Explication : les équipes **DDD**, **BBB**, **JJJ**, **GGG** et **FFF** sont les
équipes ayant réalisé le plus bas score (respectivement 3, 17, 23, 34 et, 41) et
sont alors placées dans la poule **A**. Les équipes **AAA**, **HHH** et **CCC**
sont les trois suivantes (avec pour scores 45, 63 et 64) et composeront alors
la poule **B** tandis que les équipes **KKK**, **III** et **EEE** (avec pour
scores 70, 86 et 98) seront dans la poule **C** au premier tour.

Ainsi pour le second tour, l'équipe **DDD** ira dans la poule **A**, **BBB**
dans la poule **B**, **JJJ** dans la poule **C**, **GGG** dans la poule **A**,
**FFF** dans la poule **B**, **AAA** dans la poule **C**, **HHH** dans la poule
**A**, **CCC** dans la poule **B**, **KKK** dans la poule **C**, **III** dans
la poule **A** et enfin **EEE** également dans la poule **A** puisqu'elle y est
forcée.

Pour ce qui est de l'ordre de passage :

.. table:: Exemple d'ordre de passage pour le premier tour

   +--------------------------------+-----------------------------+-----------------+-----------------+
   | Poule                          |           Poule A           |     Poule B     |     Poule C     |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Équipe                         | DDD | BBB | JJJ | GGG | FFF | AAA | HHH | CCC | KKK | III | EEE |
   +================================+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+
   | Score de dé                    |  3  |  17 |  23 |  34 |  41 |  45 |  63 |  64 |  70 |  86 |  98 |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Score de dé fois 27 modulo 100 |  81 |  59 |  21 |  18 |  7  |  15 |  1  |  28 |  90 |  22 |  46 |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Ordre de passage dans la poule |  5  |  4  |  3  |  2  |  1  |  2  |  1  |  3  |  3  |  1  |  2  |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+

.. table:: Exemple d'ordre de passage pour le second tour

   +--------------------------------+-----------------------------+-----------------+-----------------+
   | Poule                          |           Poule A           |     Poule B     |     Poule C     |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Équipe                         | DDD | GGG | HHH | III | EEE | BBB | FFF | CCC | JJJ | AAA | KKK |
   +================================+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+
   | Score de dé                    |  3  |  34 |  63 |  86 |  98 |  17 |  41 |  64 |  23 |  45 |  70 |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | Ordre de passage dans la poule |  1  |  2  |  3  |  4  |  5  |  1  |  2  |  3  |  1  |  2  |  3  |
   +--------------------------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+


Pour la finale nationale, les ordres de passage et les répartitions dans les poules
du second tour sont décidé⋅es dans l'ordre décroissant des résultats obtenus au
premier tour. Si par exemple l'équipe **AAA** a fini en tête du premier tour, elle
sera alors la première à passer dans la poule **A** du second tour.


Tirage des problèmes
~~~~~~~~~~~~~~~~~~~~

Une fois les équipes réparties dans les poules, on tire au sort les problèmes. Ce
tirage se déroule poule par poule.

Au début du tirage au sort des problèmes d'une poule, les équipes de la poule
commencent par tirer un nouveau dé à 100 faces. S'il y a des égalités, alors
les équipes concernées relancent leur dé jusqu'à ne plus avoir d'égalité.
Les équipes sont triées dans l'ordre décroissant de leur score de dé. L'équipe
ayant tiré le plus gros score est invitée à tirer son problème en première.

Il y a deux contraintes de tirage :

* Il n'est pas possible de choisir le même problème qu'une autre équipe de la poule ;
* Il n'est pas possible de défendre le même problème pour les deux jours.

À noter qu'il n'est pas impossible de tirer et de choisir un problème qui n'a
pas été traité par l'équipe, bien que cela est fortement non recommandé.

Une fois l'ordre de tirage établi, les équipes tirent tour à tour leur problème,
tant qu'elle ne l'ont pas encore choisi. Pour cela, l'équipe active tire au sort
un problème. Il est attendu que le problème tiré garantisse les deux contraintes,
et que donc il n'est pas possible de tirer un problème déjà choisi par une autre
équipe, ou bien le problème défendu lors du tour 1 si on est au tour 2.

L'équipe a désormais deux choix :

* Accepter le problème. Dans ce cas, ce sera ce problème qu'elle défendra, et
  son tirage est termé.
* Refuser le problème. Dans ce cas, la main passe à l'équipe suivante, selon le
  score des dés. Exception : si le problème tiré est un problème qui a déjà été
  refusé auparavant, alors dans ce cas l'équipe peut immédiatement tirer un
  nouveau problème (mais elle a tout de même le choix de l'accepter ou de le
  refuser).

**Attention :** si une équipe refuse trop de problèmes, alors elle pourra être
pénalisée. Chaque équipe a droit à ``P - 5`` refus sans pénalités, où ``P``
est le nombre de problèmes disponibles cette année. Par exemple, s'il y a
8 problèmes cette année, alors les équipes ont droit à 3 refus sans pénalités.
Seuls les refus distincts comptent : refuser une deuxième fois un problème
déjà refusé ne compte pas. Au-delà de ces refus gratuits, l'équipe se verra
dotée d'une pénalité de 25 % sur le coefficient de l'oral de défense, par
refus. Par exemple, si une équipe refuse 4 problèmes avec un coefficient
sur l'oral de défense normalement à ``1.6``, son coefficient passera à ``1.2``.

Une fois que toutes les équipes de la poule ont tiré leur problème, on passe
à la poule suivante. Une fois que toutes les poules ont vu leurs problèmes
tirés, on recommence pour le second tour, à partir de la première poule (déjà
définie), sauf pour la finale nationale. Le tirage au sort est terminé lorsque
toutes les poules ont vu leurs problèmes tirés pour tous les tours.


Récupération des solutions
~~~~~~~~~~~~~~~~~~~~~~~~~~

Les solutions défendues pour le premier tour dans une poule sont immédiatement
accessibles après le tirage au sort aux équipes qui doivent opposer ou rapporter
ces solutions. Pour le second tour, elles ne seront disponibles qu'après la fin
du premier tour.

Elles seront également envoyées par les organisateurices localaux, dans les mêmes
délais.


Tirage au sort sur la plateforme
--------------------------------

Le tirage est sort est géré directement sur la plateforme, de façon intuitive,
ergonomique et dynamique.

Il est accessible à l'adresse `<https://inscription.tfjm.org/draw/>`_, ou bien
en accédant à l'onglet « Tirage au sort » sur le bandeau de navigation, pourvu
d'être organisateurice ou bien d'être dans une équipe validée.


Présentation de l'interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'interface est divisée en 5 sections :

* Le nom du tournoi dans lequel se passe le tirage au sort ;
* Les derniers résultats de dés par équipe ;
* Le récapitulatif du tirage au sort en cours ;
* L'étape actuelle ;
* Les tableaux de passage par poule.

.. figure:: _static/img/draw_general.png
   :alt: Interface générale du tirage au sort

   Interface générale du tirage au sort

Sur cette capture d'écran, on voit par exemple que l'on est actuellement dans le
tournoi de Strasbourg. Le tirage du tour 1 est déjà terminé, et on est en train
de tirer la poule B2.

Nom du tournoi
..............

Les différents onglets représentent les différents tournois.

Pour une équipe participante ou un⋅e organisateurice local⋅e, il peut n'y avoir
qu'un seul tournoi, celui qui concerne l'utilisateurice.

Les administrateurices ont accès à tous les onglets, ce qui peut permettre de
passer d'un tirage au sort à un autre facilement, en un clic et sans délai.

.. figure:: _static/img/draw_tournament_tabs.png
   :alt: Onglets des différents tirages au sort par tournoi

   Le tournoi de Strasbourg est le tournoi sélectionné dans cet exemple.


Derniers résultats de dés
.........................

Les derniers jets de dés sont affichés dans cette section, avec le trigramme de
chaque équipe et son score de dé s'il existe. Le score est vert si l'équipe a
déjà tiré son dé, sinon il est jaune.

Les organisateurices peuvent cliquer sur le score de dé pour jeter un dé à la place
de l'équipe, ce qui est notamment utile à des fins de débuggage ou de souci technique
avec l'équipe en question.

.. figure:: _static/img/draw_last_rolls.png
   :alt: Derniers jets de dés

   Ici, les équipes BBB, DDD, EEE, GGG et HHH ont déjà tiré leur dé, avec pour scores
   respectifs 57, 66, 58, 41 et 68, tandis que les équipes AAA, CCC, FFF et III n'ont
   pas encore tiré leur dé.

Récapitulatif du tirage au sort
...............................

La section de gauche affiche le récapitulatif du tirage au sort en cours.
Elle n'est là qu'à des fins d'affichage, il n'est pas possible d'interagir avec.

La colonne de gauche concerne le premier tour, et la colonne de droite le second tour.
Chaque colonne est divisée en plusieurs parties, une pour chaque poule. Enfin, chaque
poule contient la liste des équipes membres, triées par ordre de passage. Elles ne sont
affichées que lorsque l'ordre de passage est déterminé.

Le tour actuel de tirage est mis en surbrillance. La poule actuelle qui réalise son
tirage est sur fond vert, l'équipe qui doit tirer son problème est sur fond bleu.

La cellule d'une équipe affiche son trigramme, son problème sélectionné pour ce tour
(s'il est déjà choisi), l'ensemble des problèmes qu'elle a refusé, et le cas échéant
ses pénalités.

.. figure:: _static/img/draw_recap.png
   :alt: Récapitulatif du tirage au sort

   Récapitulatif du tirage au sort en cours

La poule A1 est composée des équipes **GGG**, **AAA** et **III**, qui défendront
dans cet ordre. L'équipe **GGG** a tiré le problème 7, et l'a accepté. L'équipe
**AAA** a tiré le problème 3, et l'a accepté, après avoir refusé le problème 2.
L'équipe **III** a tiré le problème 1, et l'a accepté, après avoir refusé les
problèmes 3, 4, 2 et 6, et a donc une pénalité de 25 % sur son coefficient de
l'oral de défense. Notez qu'en poule B1, l'équipe **BBB** a bien pu accepter le
problème 8 après l'avoir refusé une première fois.

Dans la poule B2, actuellement en tirage, l'équipe **EEE** a déjà accepté le
problème 6, tandis que l'équipe **CCC** a refusé les problèmes 5 et 2 et que
l'équipe **AAA** a refusé le problème 2. C'est actuellement au tour de
l'équipe **AAA**, qui doit choisir d'accepter ou de refuser le problème 7
qu'elle vient de tirer.

Notez la présence du bouton « Annuler la dernière étape », visible uniquement
pour les organisateurices. Il permet d'annuler la dernière action qui vient
d'avoir lieu. Il est utile en cas de souci technique ou de mauvaise manipulation.

En cas de besoin majeur, un bouton « Annuler » est disponible en bas de l'interface
afin de réinitialiser le tirage.


Étape actuelle
..............

La partie de droite de l'écran est dédiée à l'explication de l'étape en cours du tirage
au sort. Tout est expliqué dans un encadré bleu.

À noter qu'un bouton « Exporter » peut être disponible pour les organisateurices à la
suite du tirage d'une poule. Il est utile pour valider le tirage et transmettre les
données au reste de la plateforme, débloquant notamment l'accès aux différentes solutions
pour les équipes.

Le tirage au sort est découpé en 4 phases majeures :

Composition des poules
''''''''''''''''''''''

La première phase est la composition des poules. Elle est détaillée dans la section
« Composition des poules ». Le texte affiché :

.. note::
   Nous allons commencer le tirage des problèmes.
   Vous pouvez à tout moment poser toute question si quelque chose n'est pas clair ou ne va pas.

   Nous allons d'abord tirer les poules et l'ordre de passage pour le premier tour avec toutes les équipes puis pour chaque poule, nous tirerons l'ordre de tirage pour le tour et les problèmes.

   Les capitaines, vous pouvez désormais toustes lancer un dé 100, en cliquant sur le gros bouton. Les poules et l'ordre de passage lors du premier tour sera l'ordre croissant des dés, c'est-à-dire que le plus petit lancer sera le premier à passer dans la poule A.

   Pour plus de détails sur le déroulement du tirage au sort, le règlement est accessible sur https://tfjm.org/reglement.

Un gros émoji « dé » 🎲 est affiché pour les participant⋅es, leur permettant de lancer
leur dé. Le résultat sera affiché dans la section « Derniers jets de dés », et le
bouton disparaîtra.

Les organisateurices peuvent également appuyer sur le bouton, ce qui aura pour effet
de lancer un dé pour une équipe qui n'a pas encore lancé son dé. Cela peut être utile
pour débugger ou pour aider une équipe qui a un souci technique. Rappelons toutefois
qu'il suffit de cliquer sur le score de dé d'une équipe pour lancer son dé à sa place.

.. figure:: _static/img/draw_waiting_passage_order.png
   :alt: Étape de composition des poules

   Le tirage au sort est en attente de la composition des poules.


Ordre de tirage des problèmes
'''''''''''''''''''''''''''''

Une fois les poules constituées, il faut déterminer dans quel ordre les équipes
tireront leur problème, par le biais d'un nouveau jet de dé. Un texte par exemple
peut être :

.. note::
   Nous passons au tirage des problèmes pour la poule Poule A1, entre les équipes GGG, AAA, III. Les capitaines peuvent lancer un dé 100 en cliquant sur le gros bouton pour déterminer l'ordre de tirage. L'équipe réalisant le plus gros score pourra tirer en premier.

   Pour plus de détails sur le déroulement du tirage au sort, le règlement est accessible sur https://tfjm.org/reglement.

L'émoji « dé » 🎲 n'est affiché que pour les équipes membre de la poule concernée.
Encore une fois, les organisateurices peuvent lancer le dé à la place d'une équipe
en cliquant sur son score de dé, ou bien en cliquant sur l'émoji.

.. figure:: _static/img/draw_waiting_choose_problem_order.png
   :alt: Étape de tirage de l'ordre de tirage des problèmes

   Le tirage au sort est en attente du tirage l'ordre de tirage pour la poule A1.


Tirage des problèmes
''''''''''''''''''''

Une fois l'ordre de tirage déterminé, les équipes sont invitées à tirer leurs
problèmes. Le texte affiché est par exemple :

.. note::
   C'est au tour de l'équipe GGG de choisir son problème. Cliquez sur l'urne au milieu pour tirer un problème au sort.

   Pour plus de détails sur le déroulement du tirage au sort, le règlement est accessible sur https://tfjm.org/reglement.

Un émoji « urne » 🗳️ est visible uniquement pour l'équipe active, qui doit tirer
son problème. Elle est invitée à cliquer dessus. À nouveau, les organisateurices
peuvent cliquer dessus à la place de l'équipe.

.. figure:: _static/img/draw_waiting_problem_draw.png
   :alt: Étape de tirage des problèmes

   Le tirage au sort est en attente du tirage du problème de l'équipe GGG.


Choix du problème
'''''''''''''''''

Une fois le problème tiré, l'équipe peut alors choisir de l'accepter ou de le
refuser. Le texte affiché est par exemple :

.. note::
   L'équipe GGG a tiré le problème 7 : Drôles de cookies. Elle peut décider d'accepter ou de refuser ce problème. Il reste 3 refus sans pénalité.

   Pour plus de détails sur le déroulement du tirage au sort, le règlement est accessible sur https://tfjm.org/reglement.

Deux boutons sont affichés sur la page, un bouton « Accepter », en vert,
et un bouton « Refuser », en rouge. L'équipe peut cliquer sur l'un ou l'autre
pour faire son choix. Les organisateurices peuvent également cliquer sur l'un
ou l'autre pour faire le choix à la place de l'équipe. Ces boutons sont
invisibles pour les autres équipes.

.. figure:: _static/img/draw_choose_problem.png
   :alt: Étape de choix du problème

   L'équipe GGG a tiré le problème 7. Elle peut choisir de l'accepter ou de le refuser.

.. TODO
.. note::
   Cette section sera mise à jour plus tard.


Tableaux de passage par poule
.............................

Ces tableaux, actualisés en temps réel, permettent d'afficher quels seront les
différents passages lors d'une poule. En particulier, quelle équipe défendra
quel problème, et quelle équipe opposera et laquelle rapportera.

.. figure:: _static/img/draw_passage_tables.png
   :alt: Tableaux de passage par poule

   Tableaux de passage par poule


Déroulement du tirage
~~~~~~~~~~~~~~~~~~~~~

Cette section décrit le déroulement du tirage au sort sur la plateforme, sans
rentrer dans les détails théoriques. Si la partie théorique vous intéresse,
rendez-vous à la section `Principe`_.

Démarrage du tirage au sort
...........................

Si le tirage n'a pas encore commencé, un message d'alerte s'affiche.

Les organisateurices peuvent lancer le tirage au sort en cliquant sur le bouton
« Démarrer ! ». Iels doivent d'abord paramétrer le format du tirage, en indiquant
le nombre d'équipe par poule, en séparant les nombres par des plus. Par exemple,
pour un tournoi de 9 équipes avec 3 poules de 3 équipes, on écrira ``3+3+3``.

.. figure:: _static/img/draw_start.png
   :alt: Création du tirage au sort

   Le formulaire de lancement du tirage au sort. Ici, on souhaite un tirage
   au sort avec 3 poules de 3 équipes, pour le tournoi de Strasbourg.

Attention : si toutes les poules n'ont pas la même capacité, il est essentiel de
mettre les poules dans l'ordre décroissant de capacité. Par exemple, pour un tournoi
de 8 équipes avec une poule de 5 équipes et une poules de 3 équipes, il faut
écrire ``5+3``. De plus, il n'est pas possible d'avoir 2 poules de 5 équipes.


Composition des poules
......................

Les capitaines d'équipe sont invités à lancer un dé à 100 faces. Pour cela, ils
peuvent cliquer sur le gros bouton avec l'émoji « dé » 🎲 sur la droite de l'écran.
Le résultat est affiché dans la section « Derniers jets de dés ».

.. figure:: _static/img/draw_waiting_passage_order_full.png
   :alt: Étape de composition des poules

   Le tirage au sort est en attente de la composition des poules.
   Les équipes **AAA**, **CCC** et **EEE** ont déjà lancé leur dé, avec pour
   scores 66, 34 et 97.

Une fois le bouton cliqué, il disparaît.

Les organisateurices peuvent également cliquer sur le score de dé d'une équipe
pour lancer le dé à sa place. Cela peut être utile pour débugger ou pour aider
une équipe qui a un souci technique. Il est possible de cliquer sur le dé, ce
qui a pour effet de lancer le dé pour l'équipe qui n'a pas encore tiré son dé.

Les poules sont constituées dès que toutes les équipes ont tiré leur dé, selon
le principe énoncé dans la partie `Principe`_. S'il y a des égalités, alors les
équipes concernées relancent leur dé jusqu'à ne plus avoir d'égalité.


Ordre de tirage des problèmes
.............................

Une fois les poules constituées, il faut déterminer dans quel ordre les équipes
tireront leur problème, par le biais d'un nouveau jet de dé. Les équipes sont
invitées à lancer un dé à 100 faces. Pour cela, elles peuvent cliquer sur le
gros bouton avec l'émoji « dé » 🎲 sur la droite de l'écran. Le résultat est
affiché dans la section « Derniers jets de dés ».

.. figure:: _static/img/draw_waiting_choose_problem_order_full.png
   :alt: Étape de tirage de l'ordre de tirage des problèmes

   Le tirage au sort est en attente du tirage l'ordre de tirage pour la poule A1.
   L'équipe **BBB** a déjà tiré son dé, avec pour score 56. Les équipes **CCC**
   et **DDD** sont alors attendues.

Une fois le bouton cliqué, il disparaît.

À nouveau, les organisateurices peuvent cliquer sur le score de dé d'une équipe
pour lancer le dé à sa place. Cela peut être utile pour débugger ou pour aider
une équipe qui a un souci technique. Il est possible de cliquer sur le dé, ce
qui a pour effet de lancer le dé pour l'équipe qui n'a pas encore tiré son dé.

Si deux équipes réalisent le même score, alors les équipes concernées relancent
leur dé jusqu'à ne plus avoir d'égalité.


Tirage des problèmes
....................

Les équipes membre de la poule active sont invitées à tirer leur problème, dans
l'ordre déterminé par les dés précédents.

L'équipe active est invitée à cliquer sur l'urne 🗳️ sur la droite de l'écran pour
tirer un problème au sort.

.. figure:: _static/img/draw_waiting_problem_draw_full.png
   :alt: Étape de tirage des problèmes

   L'équipe **DDD** est la première à tirer son problème, puisqu'elle a réalisé
   le plus gros score. Elle est mise en surbrillance.

Les organisateurices peuvent également cliquer sur l'urne pour lancer le dé à la
place de l'équipe. Cela peut être utile pour débugger ou pour aider une équipe qui
a un souci technique.

Le problème tiré ne peut pas être le même que celui d'une autre équipe de la poule,
ni le même que celui défendu par l'équipe lors du premier tour si on est au second
tour. Il peut en revanche être un problème déjà refusé auparavant.


Choix du problème
.................

Une fois le problème tiré, l'équipe peut alors choisir de l'accepter ou de le
refuser. Elle est invitée à cliquer sur le bouton « Accepter » ou « Refuser »
pour faire son choix.

.. figure:: _static/img/draw_choose_problem_full.png
   :alt: Étape de choix du problème

   L'équipe **DDD** a tiré le problème 3. Elle peut choisir de l'accepter ou de le refuser.

Si elle accepte le problème, alors elle a terminé, et la main passe à l'équipe
suivante. Ce sera ce problème qu'elle défendra.

Si elle refuse le problème, alors :

* Soit le problème n'avait pas encore été refusé. Dans ce cas, la main passe à
  l'équipe suivante, selon l'ordre de tirage des problèmes. Si le nombre de refus
  dépasse ``P - 3`` où ``P`` est le nombre de problèmes, alors l'équipe se verra
  dotée d'une pénalité de 25 % sur son coefficient de l'oral de défense. Ces
  pénalités sont cumulables.
* Soit le problème avait déjà été refusé. Dans ce cas, l'équipe peut revenir sur
  sa décision et accepter le problème, ou bien le rejeter gratuitement et tirer
  immédiatement un nouveau problème. Cela ne compte pas comme un refus
  supplémentaire et ne peut ajouter de pénalité.

Les organisateurices peuvent à nouveau cliquer sur les boutons à la place de
l'équipe. Cela peut être utile pour débugger ou pour aider une équipe qui a un
souci technique.

.. figure:: _static/img/draw_example.png
   :alt: Exemple de tirage de la poule A1

   Dans cet exemple, l'équipe **DDD** a tiré le problème 3, et l'a accepté.
   L'équipe **BBB** a d'abord refusé le problème 6, et a accepté plus tard
   le problème 5. L'équipe **CCC**, à qui c'est le tour, a refusé les problèmes
   8, 4, 2 et 6. Puisqu'il y a 8 problèmes, elle a une pénalité de 25 % sur son
   coefficient de l'oral de défense. Elle vient de tirer le problème 2, qu'elle
   avait déjà refusé. Elle est donc libre de le refuser à nouveau sans pénalité
   supplémentaire, ou bien de l'accepter, comme indiqué dans l'encadré à droite.

Lorsque toutes les équipes de la poule ont accepté leur problème, on passe à la
poule suivante, en revenant à l'étape de tirage au sort de l'ordre des problèmes.
Lorsqu'un tour est terminé, on passe au tour suivant, à la première poule.

Exception : pour la finale, on ne tire au sort que le premier tour.

.. figure:: _static/img/draw_end_round_1.png
    :alt: Fin du tirage au sort du premier tour

    Toutes les équipes ont tiré leur problème pour le premier tour. On passe
    au second tour, à la poule A2.


Fin du tirage au sort
.....................

Le tirage se termine lorsque toutes les équipes se sont vues attribuer un problème
pour chacun des tours.

Les organisateurices peuvent alors cliquer sur le bouton « Exporter » pour valider
le tirage et transmettre les données au reste de la plateforme, débloquant notamment
l'accès aux différentes solutions pour les équipes. Attention : cette opération n'est
pas réversible facilement.

Spécificité de la finale
........................

Pour la finale, le tirage au sort est légèrement différent. Seul le premier tour
est tiré au sort initialement. Pour le second tour, les poules et ordres de passage
sont déterminés selon le classement du premier tour.

Avant de reprendre le tirage au sort du second tour, il est essentiel que les notes
du premier tour soient rentrées correctement. En effet, ce sont ces scores qui
détermineront les poules et l'ordre de passage pour le second tour.

Pour lancer le second tour, un⋅e organisateurice doit cliquer sur le bouton
« Continuer ». Le tirage reprend ensuite normalement, à partir de la poule **A2**.

.. danger::
   À terme, il sera possible de réaliser ce second tirage au sort IRL, et de rentrer
   facilement les données au fur et à mesure du tirage.


Annulation d'une étape
......................

Il est possible d'annuler la dernière étape du tirage au sort. Cela peut être utile
en cas de souci technique ou de mauvaise manipulation. Cette option est uniquement
réservée aux organisateurices.

Pour cela, il suffit de cliquer sur le bouton « Annuler la dernière étape » dans
la partie « Récapitulatif ». Cela annule immédiatement la dernière action. Il est
possible de continuer à remonter le temps ainsi.

En cas de plus gros problème, il est possible de cliquer sur le bouton « Annuler »
en bas de page. Cela réinitialise le tirage au sort, et permet de recommencer
depuis le début. Cette suppression est irréversible, soyez sûr⋅es de ce que vous
faites avant de cliquer dessus.
