Utiliser la plateforme
======================

.. contents::

Cette plateforme est conçue pour que les différentes équipes puissent s'inscrire au TFJM² et envoyer
leurs solutions.

Si vous êtes ici, c'est que vous avez des questions sur l'utilisation du site en tant que participant⋅e.

Les inscriptions ouvrent dans le courant du mois de janvier, généralement une semaine après la publication
des problèmes. Pour l'édition 2024, les inscriptions ouvrent le 17 janvier 2024.

Tout se passe sur le site https://inscription.tfjm.org/.


S'inscrire
----------

Il est important de noter que chaque personne d'une équipe doit s'inscrire, y compris les encadrant⋅es.

Rendez-vous sur le site d'inscription, bouton « S'inscrire » en haut à droite.

Les informations suivantes sont requises pour tout le monde :

* Prénom d'usage
* Nom de famille (ou d'usage)
* Adresse électronique (sera vérifiée)
* Mot de passe et confirmation
* Rôle (participant⋅e ou encadrant⋅e)
* Date de naissance
* Genre
* Adresse postale
* Numéro de téléphone de contact
* Problèmes de santé à déclarer (allergies,…)
* Contraintes de logement à déclarer (problèmes médicaux, contraintes horaires, questions de genre,…)
* Donne son consentement pour se faire recontacter par Animath

Informations demandées exclusivement aux élèves :

* Classe (seconde ou avant/première/terminale)
* Établissement scolaire
* Nom d'un⋅e responsable légal⋅e
* Numéro de téléphone d'un⋅e responsable légal⋅e
* Adresse e-mail d'un⋅e responsable légal⋅e

Informations exclusivement demandées aux encadrant⋅es :

* Activité professionnelle

Une fois inscrit⋅e, vous recevrez par mail un lien de confirmation, qu'il vous faudra cliquer.


Connexion
---------

Une fois inscrit⋅e, vous pouvez vous connecter en utilisant le bouton en haut à droite.

Dans le champ « nom d'utilisateur », rentrez votre adresse mail. Si vous avez oublié votre mot de
passe, un formulaire est disponible pour vous aider à le réinitialiser. Vous pouvez ensuite vous
connecter. Votre prénom et votre nom apparaîtra en haut à droite.


Créer une équipe
----------------

Il suffit d'une seule personne (participant⋅e ou encadrant⋅e) pour créer une équipe. Pour créer une
équipe, il faut cliquer sur le bouton « créer une équipe ». Un nom d'équipe et un trigramme seront
demandés. Le trigramme est composé de 3 lettres majuscules, c'est ce qui permettra aux
organisateur⋅rices d'identifier rapidement votre équipe.

.. image:: /_static/img/create_team.png
   :alt: Création d'une équipe

Une fois l'équipe créée, vous obtenez un code à 6 caractères, lettres ou chiffre. Ce code est à
transettre à l'ensemble des membres de votre équipe (et seulement à elleux).

.. image:: /_static/img/team_info.png
   :alt: Information sur l'équipe nouvellement créée


Rejoindre une équipe
--------------------

Si l'équipe est déjà créée, vous aurez besoin du code d'accès transmis par la personne ayant créé
l'équipe. Vous pouvez cliquer sur « Rejoindre une équipe », et entrer le code.

.. image:: /_static/img/join_team.png
   :alt: Rejoindre une équipe par son code d'accès

Vous avez désormais normalement rejoint l'équipe.

En cas de problème, ou si vous ne savez pas de quel code on parle, contactez-nous à l'adresse
contact@tfjm.org.


Informations sur les tournois
-----------------------------

Les tournois peuvent être trouvés dans l'onglet « Tournois ». Vous avez accès, pour chaque tournoi,
à l'ensemble des dates importantes et les lieux des tournois.

.. image:: /_static/img/tournament_info.png
   :alt: Informations sur un tournoi

Davantage d'informations peuvent être trouvées sur le site vitrine : https://tfjm.org/infos-tournois/.


Choisir un tournoi
------------------

Pour accéder aux paramètres de votre équipe, vous pouvez aller sur l'onglet « Mon équipe », dans la
barre de navigation.

Pour choisir votre tournoi, il vous suffit de vous rendre sur la page de votre équipe et de cliquer
sur « Modifier ». Un formulaire vous permet alors de choisir votre tournoi.

.. image:: /_static/img/choose_tournament.png
   :alt: Formulaire de mise à jour de l'équipe permettant de choisir un tournoi

Attention cependant : cela ne confirme pas votre inscription. Vous devez pour cela envoyer l'ensemble
de vos documents (voir ci-dessous).


Transmettre ses documents
-------------------------

Pour valider votre inscription, vous devez :

* Avoir choisi un tournoi ;
* Que chaque membre de l'équipe ait transmis :

   * Autorisation de droit à l'image ;
   * Fiche sanitaire de liaison et carnet de vaccination (pour les mineur⋅es) ;
   * Autorisation parentale (pour les mineur⋅es) ;

* Transmettre une lettre de motivation.

La lettre de motivation doit être envoyée une seule fois pour toute l'équipe, peut être envoyée
depuis l'interface « Mon équipe », au format PDF, dont le contenu est défini dans le
règlement : https://tfjm.org/reglement/.

Concernant les documents personnels, ils peuvent être envoyés depuis le menu « Mon compte », qui
peut être trouvé en haut à droite dans la barre de navigation. Chaque fichier doit être envoyé
au format PDF et peser moins de 2 Mo.

.. image:: /_static/img/user_info.png
   :alt: Informations sur l'utilisateur⋅rice

En cas de besoin, contactez-nous à l'adresse contact@tfjm.org.


Valider son équipe
------------------

Pour prétendre à la validation, il faut que l'équipe compte au moins 1 encadrant⋅e et 4 participant⋅es.
Il faut ensuite que la lettre de motivation soit transmise, le tournoi choisi et que tous les documents
nécessaires ont été transmis (voir section précédente).

Une fois tous les prérequis réunis, sur la page « Mon équipe », il est possible de cliquer sur le bouton
pour demander la validation.

.. image:: /_static/img/validate_team.png
   :alt: Formulaire de validation d'équipe

.. warning::
   Les places étant limitées, rien ne garantit que vous pourrez avoir votre place dans le tournoi. Nous
   vous encourageons à respecter un maximum les critères définis dans le règlement :
   https://tfjm.org/reglement/. Selon les disponiblités et votre position géographique, il pourra
   vous être proposé de participer à un tournoi voisin.

   Une fois les deadlines dépassées, rien ne vous garantit une place au TFJM², alors attention aux dates.

Vous recevrez par mail une réponse des organisateur⋅rices locaux⋅ales. En cas de besoin, contactez-nous
à l'adresse contact@tfjm.org.


Payer son inscription
---------------------

Une fois votre inscription validée, il vous faudra payer votre participation. Les frais s'élèvent à
21 € par élève, sauf pour les élèves boursièr⋅es qui en sont exonéré⋅es. Les encadrant⋅es n'ont pas
à payer. Pour la finale, les frais sont de 35 € par élève.

.. note::
   Ces frais couvrent une partie des frais de restauration et d'hébergement. L'organisation reste
   bénévole.

Il est possible de payer par carte bancaire ou virement bancaire. Pour d'autres types de paiement,
merci de nous contacter.

Pour payer, si votre équipe est bien validée, vous pouvez vous rendre sur la page de votre compte
ou celle de votre équipe, et cliquer sur le bouton « Modifier le paiement », qui devrais désormais
apparaître. Vous pouvez également utiliser le lien présent dans le volet « Informations ».

.. image:: /_static/img/payment_index.png
   :alt: Page de paiement

.. note::

   Vous recevrez un mail de rappel chaque semaine. Le paiement doit être effectué avant le début du
   tournoi, sans quoi votre participation pourrait être refusée. En cas de difficultés de paiement,
   merci de nous contacter.

Carte bancaire
""""""""""""""

La façon la plus simple de payer son inscription est de payer par carte bancaire. Animath utilise
`Hello Asso <https://helloasso.com/>`_ en guise de solution de paiements en ligne.

Il vous suffit de cliquer sur le bouton « Aller à la page Hello Asso ». Vous serez redirigé⋅e ensuite
vers la page de paiement.

.. warning::

   Pour procéder au paiement, si vous êtes mineur⋅e, vous devrez demander à un⋅e adulte de payer à
   votre place. Il est important dans la suite de bien mettre les coordonnées du payeur ou de la payeuse,
   majeur⋅e, et non celles de l'élève.

.. image:: /_static/img/payment_hello_asso_step_1.png
   :alt: Formulaire de paiement Hello Asso

La personne qui paie peut rentrer ses informations demandées (nom, prénom, e-mail, date de naissance).

Notez que, par défaut, Hello Asso ajoute automatiquement une participation à ses frais de fonctionnement,
d'environ 15 à 20 % du prix payé. Ces frais ne sont pas obligatoires, ne sont pas versés à Animath et
représentent la seule source de revenus à Hello Asso. En effet : Animath ne verse aucune commission lors
de ses transactions, et seules les contributions volontaires financent leur service.

Sur la page suivante, vous pouvez indiquer vos coordonnées bancaires :

.. image:: /_static/img/payment_hello_asso_step_2.png
   :alt: Formulaire de paiement Hello Asso - coordonnées bancaires

Vous devez ensuite éventuellement confirmer votre paiement auprès de votre banque.

Une fois ceci fait, vous êtes automatiquement redirigé⋅es vers la plateforme du TFJM² :

.. image:: /_static/img/payment_hello_asso_confirmation.png
   :alt: Confirmation de paiement Hello Asso

Il se peut que la validation ne soit pas instantanée. Elle peut prendre au plus quelques minutes.
Si le délai est plus long, merci de nous contacter.

Vous recevrez ensuite un mail de confirmation de la plateforme, ainsi qu'un justificatif de paiement
de la part de Hello Asso.


Carte bancaire - paiement par un tiers
""""""""""""""""""""""""""""""""""""""

Il est possible, si nécessaire, de faire payer l'inscription par carte bancaire par un tiers. Pour cela,
vous pouvez lui transmettre le lien de paiement qui apparaît au centre de l'écran. Cela est notamment
utile pour faire payer l'inscription par un établissement scolaire, ou par des parents.

L'interface de paiement sera ensuite identique.


Virement bancaire
"""""""""""""""""

Il est possible de payer par virement bancaire. Pour cela, vous pouvez ouvrir l'onglet virement bancaire :

.. image:: /_static/img/payment_bank_transfer.png
   :alt: Formulaire de paiement par virement bancaire

Pour effectuer le virement, merci de mettre en référence du virement « TFJMpu » suivi du nom et du prénom de l'élève.

Les coordonnées bancaires sont :

* IBAN : FR76 1027 8065 0000 0206 4290 127
* BIC : CMCIFR2A

Une fois le paiment effectué, vous pouvez envoyer une preuve de virement via le formulaire ci-dessus. Le paiement
sera ensuite validé manuellement par les organisateur⋅rices après réception.

Si vous avez besoin d'une facture, merci de nous contacter.


Exonération - boursièr⋅es
"""""""""""""""""""""""""

Si vous bénéficiez d'une bourse, vous pouvez être exonéré⋅es des frais de participation. Pour cela, il vous suffit
de nous envoyer une copie de votre notification de bourse, ou tout autre document justifiant de votre situation.
Vous pouvez envoyer ce document en vous rendant sur l'onglet dédié :

.. image:: /_static/img/payment_scholarship.png
   :alt: Formulaire de soumission de notification de bourse


Paiements groupés
"""""""""""""""""

Il est possible de payer en une seule fois pour toute l'équipe. Cela est notamment utile si l'inscription est
payée par l'établissement. Pour cela, il suffit de cliquer sur le bouton « Regrouper les paiements de mon équipe ».
Cela a pour effet d'unifier les paiements de l'équipe, et de ne pas demander à chaque membre de payer individuellement.
Attention : cette fonction n'est possible que si aucun membre de l'équipe n'a encore payé son inscription.

.. image:: /_static/img/payment_grouped.png
   :alt: Page de paiement groupé


Envoyer ses solutions
---------------------

.. TODO
.. note::
   Cette section sera mise à jour plus tard.


Participer au tirage au sort
----------------------------

La documentation des tirages au sort est disponible sur `la page dédiée <draw.html>`_.


Envoyer ses notes de synthèse
-----------------------------

.. TODO
.. note::
   Cette section sera mise à jour plus tard.


Récupérer les solutions adverses
--------------------------------

.. TODO
.. note::
   Cette section sera mise à jour plus tard.
