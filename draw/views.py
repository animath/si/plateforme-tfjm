# Copyright (C) 2023 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from participation.models import Tournament


class DisplayView(LoginRequiredMixin, TemplateView):
    """
    This view is the main interface of the drawing system, which is working
    with Javascript and websockets.
    """
    template_name = 'draw/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        reg = self.request.user.registration
        if reg.is_admin:
            # Administrators can manage all tournaments
            tournaments = Tournament.objects.order_by('id').all()
        elif reg.is_volunteer:
            # A volunteer can see their tournaments
            tournaments = reg.interesting_tournaments
        else:
            if not reg.team:
                raise PermissionDenied(_("You are not in a team."))

            # A participant can see its own tournament, or the final if necessary
            tournaments = [reg.team.participation.tournament] if reg.team.participation.valid else []
            if reg.team.participation.final:
                tournaments.append(Tournament.final_tournament())

        context['tournaments'] = tournaments
        # This will be useful for JavaScript data
        context['tournaments_simplified'] = [{'id': t.id, 'name': t.name} for t in tournaments]
        context['problems'] = settings.PROBLEMS

        context['range_100'] = range(0, 100, 10)
        context['range_10'] = range(0, 10, 1)

        return context
