# Copyright (C) 2023 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from .views import DisplayView


app_name = "draw"

urlpatterns = [
    path('', DisplayView.as_view(), name='index'),
]
