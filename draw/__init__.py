# Copyright (C) 2023 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

default_app_config = 'draw.apps.DrawConfig'
