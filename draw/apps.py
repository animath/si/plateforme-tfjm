# Copyright (C) 2023 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DrawConfig(AppConfig):
    name = 'draw'
    verbose_name = _("Draw")
