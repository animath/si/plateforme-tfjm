# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Draw, Pool, Round, TeamDraw


class RoundInline(admin.TabularInline):
    model = Round
    extra = 0
    autocomplete_fields = ('draw', 'current_pool',)
    show_change_link = True


class PoolInline(admin.TabularInline):
    model = Pool
    extra = 0
    autocomplete_fields = ('round', 'current_team', 'associated_pool',)
    show_change_link = True


class TeamDrawInline(admin.TabularInline):
    model = TeamDraw
    extra = 0
    autocomplete_fields = ('participation', 'round', 'pool',)
    show_change_link = True


@admin.register(Draw)
class DrawAdmin(admin.ModelAdmin):
    list_display = ('tournament', 'teams', 'current_round', 'get_state',)
    list_filter = ('tournament', 'current_round__number',)
    search_fields = ('tournament__name', 'tournament__participation__team__trigram',)
    autocomplete_fields = ('tournament',)
    inlines = (RoundInline,)

    @admin.display(description=_("teams"))
    def teams(self, record: Draw):
        return ', '.join(p.team.trigram for p in record.tournament.participations.filter(valid=True).all())


@admin.register(Round)
class RoundAdmin(admin.ModelAdmin):
    list_display = ('draw', 'tournament', 'number', 'teams',)
    list_filter = ('draw__tournament', 'number',)
    search_fields = ('draw__tournament__name', 'pool__teamdraw__participation__team__trigram')
    ordering = ('draw__tournament__name', 'number')
    autocomplete_fields = ('draw', 'current_pool',)
    inlines = (PoolInline,)

    @admin.display(description=_("tournament"), ordering='draw__tournament__name')
    def tournament(self, record):
        return record.draw.tournament

    @admin.display(description=_("teams"))
    def teams(self, record: Round):
        return ', '.join(td.participation.team.trigram for td in record.team_draws)


@admin.register(Pool)
class PoolAdmin(admin.ModelAdmin):
    list_display = ('tournament', 'round', 'letter', 'teams')
    list_filter = ('round__draw__tournament', 'round__number', 'letter')
    ordering = ('round__draw__tournament__name', 'round', 'letter')
    search_fields = ('round__draw__tournament__name', 'teamdraw__participation__team__trigram',)
    autocomplete_fields = ('round', 'current_team', 'associated_pool',)
    inlines = (TeamDrawInline,)

    @admin.display(ordering='round__draw__tournament__name', description=_("tournament"))
    def tournament(self, record):
        return record.round.draw.tournament

    @admin.display(description=_("teams"))
    def teams(self, record: Round):
        return ', '.join(td.participation.team.trigram for td in record.team_draws)


@admin.register(TeamDraw)
class TeamDrawAdmin(admin.ModelAdmin):
    list_display = ('participation', 'tournament', 'view_round', 'pool', 'accepted', 'rejected',
                    'passage_index', 'choose_index', 'passage_dice', 'choice_dice',)
    list_filter = ('round__draw__tournament', 'round__number', 'pool__letter',)
    search_fields = ('round__draw__tournament__name', 'participation__team__trigram',)
    autocomplete_fields = ('participation', 'round', 'pool',)

    @admin.display(ordering='round__draw__tournament__name', description=_("tournament"))
    def tournament(self, record):
        return record.round.draw.tournament

    @admin.display(ordering='round__number', description=_('round'))
    def view_round(self, record):
        return record.round.get_number_display()
