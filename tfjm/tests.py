# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from django.test import TestCase


class TestLoadModules(TestCase):
    """
    Load modules that are not used in development mode in order to increase coverage.
    """
    def test_asgi(self):
        from tfjm import asgi
        self.assertIsNotNone(asgi)

    def test_wsgi(self):
        from tfjm import wsgi
        self.assertIsNotNone(wsgi)

    def test_load_production_settings(self):
        os.putenv("TFJM_STAGE", "prod")
        os.putenv("DJANGO_DB_TYPE", "postgres")
        from tfjm import settings_prod
        self.assertFalse(settings_prod.DEBUG)
