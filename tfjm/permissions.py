# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db import models
from django.utils.translation import gettext_lazy as _


class PermissionType(models.TextChoices):
    ANONYMOUS = 'anonymous', _("Everyone, including anonymous users")
    AUTHENTICATED = 'authenticated', _("Authenticated users")
    VOLUNTEER = 'volunteer', _("All volunteers")
    TOURNAMENT_MEMBER = 'tournament', _("All members of a given tournament")
    TOURNAMENT_ORGANIZER = 'organizer', _("Tournament organizers only")
    TOURNAMENT_JURY_PRESIDENT = 'jury_president', _("Tournament organizers and jury presidents of the tournament")
    JURY_MEMBER = 'jury', _("Jury members of the pool")
    POOL_MEMBER = 'pool', _("Jury members and participants of the pool")
    TEAM_MEMBER = 'team', _("Members of the team and organizers of concerned tournaments")
    PRIVATE = 'private', _("Private, reserved to explicit authorized users")
    ADMIN = 'admin', _("Admin users")
