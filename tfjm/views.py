# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from haystack.generic_views import SearchView


class AdminMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and not request.user.registration.is_admin:
            self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class VolunteerMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and not request.user.registration.is_volunteer:
            self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class UserMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and not user.registration.is_admin and user.pk != kwargs["pk"]:
            self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class UserRegistrationMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        user_object = User.objects.get(registration__pk=kwargs["pk"])
        if user.is_authenticated and not user.registration.is_admin and user.registration.pk != kwargs["pk"] and \
                not (user.registration.is_volunteer and user_object.registration.team is not None
                     and user_object.registration.team.participation.tournament
                     in user.registration.organized_tournaments.all()):
            self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class LoginRequiredTemplateView(LoginRequiredMixin, TemplateView):
    pass


class AdminSearchView(AdminMixin, SearchView):
    pass
