# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path
import tfjm.urls

from . import test_views

urlpatterns = tfjm.urls.urlpatterns

urlpatterns += [
    path('helloasso-test/api/oauth2/token', test_views.TestHelloAssoOAuth2View.as_view(),
         name='helloasso-test-oauth2-token'),
    path('helloasso-test/api/v5/organizations/animath/checkout-intents/',
         test_views.TestHelloAssoCheckoutIntentCreateView.as_view(),
         name='helloasso-test-checkout-intent-create'),
    path('helloasso-test/api/v5/organizations/animath/checkout-intents/<int:checkout_intent_id>/',
         test_views.TestHelloAssoCheckoutIntentDetailView.as_view(),
         name='helloasso-test-checkout-intent-detail'),
    path('helloasso-test/redirect-payment/<int:checkout_intent_id>/',
         test_views.TestHelloAssoRedirectPaymentView.as_view(),
         name='helloasso-test-redirect-payment'),
]
