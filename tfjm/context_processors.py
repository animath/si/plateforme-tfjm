# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from participation.models import Tournament


def tfjm_context(request):
    return {
        'TFJM': {
            'APP': settings.TFJM_APP,
            'APP_NAME': settings.APP_NAME,
            'HAS_OBSERVER': settings.HAS_OBSERVER,
            'HAS_FINAL': settings.HAS_FINAL,
            'HOME_PAGE_LINK': settings.HOME_PAGE_LINK,
            'LOGO_PATH': "tfjm/img/" + settings.LOGO_FILE,
            'NB_ROUNDS': settings.NB_ROUNDS,
            'ML_MANAGEMENT': settings.ML_MANAGEMENT,
            'PAYMENT_MANAGEMENT': settings.PAYMENT_MANAGEMENT,
            'RECOMMENDED_SOLUTIONS_COUNT': settings.RECOMMENDED_SOLUTIONS_COUNT,
            'REGISTRATION_DATES': settings.REGISTRATION_DATES,
            'SINGLE_TOURNAMENT': settings.SINGLE_TOURNAMENT,
            'HEALTH_SHEET_REQUIRED': settings.HEALTH_SHEET_REQUIRED,
            'VACCINE_SHEET_REQUIRED': settings.VACCINE_SHEET_REQUIRED,
            'MOTIVATION_LETTER_REQUIRED': settings.MOTIVATION_LETTER_REQUIRED,
            'SUGGEST_ANIMATH': settings.SUGGEST_ANIMATH,
        },
        'TFJM_TOURNAMENT':
            Tournament.objects.first() if Tournament.objects.exists() and settings.SINGLE_TOURNAMENT else None,
    }
