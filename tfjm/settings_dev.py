# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
