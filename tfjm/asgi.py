# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

"""
ASGI config for tfjm project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tfjm.settings')

django_asgi_app = get_asgi_application()

# useful since the import must be done after the application initialization
import tfjm.routing  # noqa: E402, I202

application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": AllowedHostsOriginValidator(
            AuthMiddlewareStack(URLRouter(tfjm.routing.websocket_urlpatterns))
        ),
    }
)
