function initModal(target, url, content_id = 'form-content') {
    document.querySelectorAll('[data-bs-target="#' + target + 'Modal"]')
      .forEach(elem => elem.addEventListener('click', () => {
        let modalBody = document.querySelector("#" + target + "Modal div.modal-body")

        if (!modalBody.innerHTML.trim()) {
            if (url instanceof Function) url = url()

            fetch(url, {headers: {'CONTENT-ONLY': '1'}})
                .then(resp => resp.text())
                .then(resp => new DOMParser().parseFromString(resp, 'text/html'))
                .then(res => modalBody.innerHTML = res.getElementById(content_id).outerHTML)
                .then(() => $('.selectpicker').selectpicker())  // TODO Update that when the library will be JQuery-free
        }
    }))
}
