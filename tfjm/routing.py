# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import chat.consumers
from django.urls import path
import draw.consumers

websocket_urlpatterns = [
    path("ws/chat/", chat.consumers.ChatConsumer.as_asgi()),
    path("ws/draw/", draw.consumers.DrawConsumer.as_asgi()),
]
