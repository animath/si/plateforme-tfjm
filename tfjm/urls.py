# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

"""tfjm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.utils.translation import gettext_lazy as _
from django.views.defaults import bad_request, page_not_found, permission_denied, server_error
from django.views.generic import TemplateView
from participation.views import MotivationLetterView
from registration.views import HealthSheetView, ParentalAuthorizationView, PhotoAuthorizationView, \
    ReceiptView, SolutionView, VaccineSheetView, WrittenReviewView

from .views import AdminSearchView

urlpatterns = [
    path('', TemplateView.as_view(template_name=f"index_{settings.TFJM_APP.lower()}.html",
                                  extra_context={'title': _("Home")}),
         name='index'),
    path('about/', TemplateView.as_view(template_name="about.html"), name='about'),
    path('i18n/', include('django.conf.urls.i18n')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls, name="admin"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('search/', AdminSearchView.as_view(), name="haystack_search"),

    path('api/', include('api.urls')),
    path('chat/', include('chat.urls')),
    path('draw/', include('draw.urls')),
    path('participation/', include('participation.urls')),
    path('registration/', include('registration.urls')),

    path('media/authorization/photo/<str:filename>/', PhotoAuthorizationView.as_view(),
         name='photo_authorization'),
    path('media/authorization/health/<str:filename>/', HealthSheetView.as_view(),
         name='health_sheet'),
    path('media/authorization/vaccine/<str:filename>/', VaccineSheetView.as_view(),
         name='vaccine_sheet'),
    path('media/authorization/parental/<str:filename>/', ParentalAuthorizationView.as_view(),
         name='parental_authorization'),
    path('media/authorization/receipt/<str:filename>/', ReceiptView.as_view(),
         name='receipt'),
    path('media/authorization/motivation_letters/<str:filename>/', MotivationLetterView.as_view(),
         name='motivation_letter'),

    path('media/solutions/<str:filename>/', SolutionView.as_view(),
         name='solution'),
    path('media/reviews/<str:filename>/', WrittenReviewView.as_view(),
         name='reviews'),
]

if settings.DEBUG:
    # Serve static files in DEBUG mode
    import django.contrib.staticfiles.urls
    urlpatterns += django.contrib.staticfiles.urls.urlpatterns

handler400 = bad_request
handler403 = permission_denied
handler404 = page_not_found
handler500 = server_error
