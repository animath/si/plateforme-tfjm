# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from .views import AddOrganizerView, AdultPhotoAuthorizationTemplateView, ChildPhotoAuthorizationTemplateView, \
    InstructionsTemplateView, MyAccountDetailView, ParentalAuthorizationTemplateView, \
    PaymentHelloAssoReturnView, PaymentRedirectHelloAssoView, PaymentUpdateGroupView, PaymentUpdateView, \
    ResetAdminView, SignupView, UserDetailView, UserImpersonateView, UserListView, UserResendValidationEmailView, \
    UserUpdateView, UserUploadHealthSheetView, UserUploadParentalAuthorizationView, UserUploadPhotoAuthorizationView, \
    UserUploadVaccineSheetView, UserValidateView, UserValidationEmailSentView

app_name = "registration"

urlpatterns = [
    path("signup/", SignupView.as_view(), name="signup"),
    path("add-organizer/", AddOrganizerView.as_view(), name="add_organizer"),
    path('validate_email/sent/', UserValidationEmailSentView.as_view(), name='email_validation_sent'),
    path('validate_email/resend/<int:pk>/', UserResendValidationEmailView.as_view(),
         name='email_validation_resend'),
    path('validate_email/<uidb64>/<token>/', UserValidateView.as_view(), name='email_validation'),
    path("user/", MyAccountDetailView.as_view(), name="my_account_detail"),
    path("user/<int:pk>/", UserDetailView.as_view(), name="user_detail"),
    path("user/<int:pk>/update/", UserUpdateView.as_view(), name="update_user"),
    path("user/<int:pk>/upload-photo-authorization/", UserUploadPhotoAuthorizationView.as_view(),
         name="upload_user_photo_authorization"),
    path("user/<int:pk>/upload-photo-authorization/final/", UserUploadPhotoAuthorizationView.as_view(),
         name="upload_user_photo_authorization_final"),
    path("parental-authorization-template/", ParentalAuthorizationTemplateView.as_view(),
         name="parental_authorization_template"),
    path("photo-authorization-template/adult/", AdultPhotoAuthorizationTemplateView.as_view(),
         name="photo_authorization_adult_template"),
    path("photo-authorization-template/child/", ChildPhotoAuthorizationTemplateView.as_view(),
         name="photo_authorization_child_template"),
    path("instructions-template/", InstructionsTemplateView.as_view(), name="instructions_template"),
    path("user/<int:pk>/upload-health-sheet/", UserUploadHealthSheetView.as_view(),
         name="upload_user_health_sheet"),
    path("user/<int:pk>/upload-vaccine-sheet/", UserUploadVaccineSheetView.as_view(),
         name="upload_user_vaccine_sheet"),
    path("user/<int:pk>/upload-parental-authorization/", UserUploadParentalAuthorizationView.as_view(),
         name="upload_user_parental_authorization"),
    path("user/<int:pk>/upload-parental-authorization/final/", UserUploadParentalAuthorizationView.as_view(),
         name="upload_user_parental_authorization_final"),
    path("update-payment/<int:pk>/", PaymentUpdateView.as_view(), name="update_payment"),
    path("update-payment/<int:pk>/toggle-group-mode/", PaymentUpdateGroupView.as_view(),
         name="update_payment_group_mode"),
    path("update-payment/<int:pk>/hello-asso/", PaymentRedirectHelloAssoView.as_view(), name="payment_hello_asso"),
    path("update-payment/<int:pk>/hello-asso/return/", PaymentHelloAssoReturnView.as_view(),
         name="payment_hello_asso_return"),
    path("user/<int:pk>/impersonate/", UserImpersonateView.as_view(), name="user_impersonate"),
    path("user/list/", UserListView.as_view(), name="user_list"),
    path("reset-admin/", ResetAdminView.as_view(), name="reset_admin"),
]
