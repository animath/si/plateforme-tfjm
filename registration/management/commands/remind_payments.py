# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.core.management import BaseCommand

from ...models import Payment


class Command(BaseCommand):
    """
    This command sends a mail to each participant who has not yet paid.
    """
    help = "Envoie un mail de rappel à toustes les participant⋅es qui n'ont pas encore payé ou déclaré de paiement."

    def handle(self, *args, **options):
        if not settings.PAYMENT_MANAGEMENT:
            return

        for payment in Payment.objects.filter(valid=False).filter(registrations__team__participation__valid=True).all():
            payment.send_remind_mail()
