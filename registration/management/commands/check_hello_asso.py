# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import json

from django.conf import settings
from django.core.management import BaseCommand

from ...models import Payment


class Command(BaseCommand):
    """
    This command checks if the initiated Hello Asso payments are validated or not.
    """
    help = "Vérifie si les paiements Hello Asso initiés sont validés ou non. Si oui, valide les inscriptions."

    def handle(self, *args, **options):
        if not settings.PAYMENT_MANAGEMENT:
            return

        for payment in Payment.objects.exclude(valid=True).filter(checkout_intent_id__isnull=False).all():
            checkout_intent = payment.get_checkout_intent()
            if checkout_intent is not None and 'order' in checkout_intent:
                payment.type = 'helloasso'
                payment.valid = True
                payment.additional_information = json.dumps(checkout_intent['order'])
                payment.save()
                payment.send_helloasso_payment_confirmation_mail()
