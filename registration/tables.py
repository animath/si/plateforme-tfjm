# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
import django_tables2 as tables
from participation.models import Team

from .models import Payment, Registration


class RegistrationTable(tables.Table):
    """
    Table of all registrations.
    """
    last_name = tables.LinkColumn(
        'registration:user_detail',
        args=[tables.A("user_id")],
        verbose_name=lambda: _("last name").capitalize(),
        accessor="user__last_name",
    )

    def order_type(self, queryset, desc):
        return queryset.order_by(('-' if desc else '') + 'polymorphic_ctype'), True

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped',
        }
        model = Registration
        fields = ('last_name', 'user__first_name', 'user__email', 'type',)
        order_by = ('type', 'last_name', 'first_name',)


class PaymentTable(tables.Table):
    """
    Table of all payments.
    """
    team_id = tables.Column(
        verbose_name=_("team").capitalize,
    )

    update_payment = tables.LinkColumn(
        'registration:update_payment',
        accessor='id',
        args=[tables.A("id")],
        verbose_name=_("Update"),
        orderable=False,
    )

    def render_team_id(self, value):
        return Team.objects.get(id=value).trigram

    def render_amount(self, value):
        return f"{value} €"

    def render_update_payment(self, record):
        return mark_safe(f"<button class='btn btn-secondary'><i class='fas fa-money-bill-wave'></i> {_('Update')}</button>")

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped',
        }
        row_attrs = {
            'class': lambda record: ('table-success' if record.valid else
                                     'table-danger' if record.valid is False else 'table-warning'),
        }
        model = Payment
        fields = ('registrations', 'team_id', 'type', 'amount', 'valid', 'update_payment',)
        empty_text = _("No payment yet.")
