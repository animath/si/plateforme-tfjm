# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from .views import PaymentViewSet, RegistrationViewSet, VolunteersViewSet


def register_registration_urls(router, path):
    """
    Configure router for registration REST API.
    """
    router.register(path + "/payment", PaymentViewSet)
    router.register(path + "/registration", RegistrationViewSet)
    router.register(path + "/volunteers", VolunteersViewSet, basename="volunteers")
