# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.models import User
from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from ..models import CoachRegistration, ParticipantRegistration, \
    Payment, StudentRegistration, VolunteerRegistration


class CoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoachRegistration
        fields = '__all__'


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParticipantRegistration
        fields = '__all__'


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRegistration
        fields = '__all__'


class VolunteerSerializer(serializers.ModelSerializer):
    class Meta:
        model = VolunteerRegistration
        fields = '__all__'


class RegistrationSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        CoachRegistration: CoachSerializer,
        StudentRegistration: StudentSerializer,
        VolunteerRegistration: VolunteerSerializer,
    }


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'


class BasicUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', ]
