# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.db.models.signals import post_save, pre_save


class RegistrationConfig(AppConfig):
    """
    Registration app contains the detail about users only.
    """
    name = 'registration'

    def ready(self):
        from registration import signals
        pre_save.connect(signals.set_username, 'auth.User')
        pre_save.connect(signals.send_email_link, 'auth.User')
        pre_save.connect(signals.update_payment_amount, 'registration.Payment')
        post_save.connect(signals.create_admin_registration, 'auth.User')
