{% load i18n %}
{% trans "Hi" %} {{ registration|safe }},

{% blocktrans trimmed with amount=payment.amount team=payment.team.trigram tournament=payment.tournament.name %}
We successfully received the payment of {{ amount }} € for your participation for the TFJM² in the team {{ team }} for the tournament {{ tournament }}!
{% endblocktrans %}

{% trans "Your registration is now fully completed, and you can work on your solutions." %}
{% trans "Be sure first that other members of your team also pay their registration." %}

{% trans "As a reminder, here are the following important dates:" %}
* {% trans "Deadline to send the solutions:" %} {{ payment.tournament.solution_limit|date }}
* {% trans "Problems draw:" %} {{ payment.tournament.solutions_draw|date }}
* {% trans "Tournament dates:" %} {% trans "From" %} {{ payment.tournament.date_start|date }} {% trans "to" %} {{ payment.tournament.date_end|date }}

{% trans "Please note that these dates may be subject to change. If your local organizers gave you different dates, trust them." %}

{% trans "NB: This mail don't represent a payment receipt. The payer should receive a mail from Hello Asso. If it is not the case, please contact us if necessary" %}

--
{% trans "The TFJM² team" %}

