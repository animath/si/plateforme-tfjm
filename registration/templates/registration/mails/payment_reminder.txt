{% load i18n %}
{% trans "Hi" %} {{ registration|safe }},

{% blocktrans trimmed with amount=payment.amount team=payment.team.trigram tournament=payment.tournament %}
You are registered for the TFJM² of {{ tournament }}. Your team {{ team }} has been successfully validated.
To end your inscription, you must pay the amount of {{ amount }} €.
{% endblocktrans %}
{% if payment.grouped %}
{% trans "This price includes the registrations of all members of your team." %}
{% endif %}
{% trans "You can pay by credit card or by bank transfer. You can read full instructions on the payment page:" %}

https://{{ domain }}{% url "registration:update_payment" pk=payment.pk %}

{% trans "If you have a scholarship, then the registration is free for you. You must then upload it on the payment page using the above link." %}

{% trans "It is also possible to allow an external person (your parents, your school, etc.) to pay for you with credit card. Instructions are also available on the payment page." %}

{% trans "If you have any problem, feel free to contact us." %}

--
The TFJM² team
