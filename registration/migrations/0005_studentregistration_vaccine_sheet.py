# Generated by Django 3.2.18 on 2023-02-19 23:38

from django.db import migrations, models
import registration.models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0004_volunteer_admin'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentregistration',
            name='vaccine_sheet',
            field=models.FileField(blank=True, default='', upload_to=registration.models.get_random_vaccine_filename, verbose_name='vaccine sheet'),
        ),
    ]
