# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from django import template


def get_env(key: str) -> str:
    return os.getenv(key)


register = template.Library()
register.filter("get_env", get_env)
