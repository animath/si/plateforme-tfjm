# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.models import User
from tfjm.lists import get_sympa_client

from .models import Registration, VolunteerRegistration


def set_username(instance, **_):
    """
    Ensure that the user username is always equal to the user email address.
    """
    instance.username = instance.email


def send_email_link(instance, **_):
    """
    If the email address got changed, send a new validation link
     and update the registration status in the team mailing list.
    """
    if instance.pk:
        old_instance = User.objects.get(pk=instance.pk)
        if old_instance.email != instance.email:
            registration = Registration.objects.get(user=instance)
            registration.email_confirmed = False
            registration.save()
            registration.user = instance
            registration.send_email_validation_link()

            if registration.participates and registration.team:
                get_sympa_client().unsubscribe(old_instance.email, f"equipe-{registration.team.trigram.lower()}", False)
                get_sympa_client().subscribe(instance.email, f"equipe-{registration.team.trigram.lower()}", False,
                                             f"{instance.first_name} {instance.last_name}")


def create_admin_registration(instance, **_):
    """
    When a super user got created through console,
    ensure that an admin registration is created.
    """
    if instance.is_superuser:
        VolunteerRegistration.objects.get_or_create(user=instance, admin=True)


def update_payment_amount(instance, **_):
    """
    When a payment got created, ensure that the amount is set.
    """
    if instance.type == 'free' or instance.type == 'scholarship':
        instance.amount = 0
    elif instance.pk and instance.registrations.exists():
        instance.amount = instance.registrations.count() * instance.tournament.price
