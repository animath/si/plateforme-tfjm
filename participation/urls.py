# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from .views import CreateTeamView, FinalNotationSheetTemplateView, GSheetNotificationsView, JoinTeamView, \
    MyParticipationDetailView, MyTeamDetailView, NotationSheetsArchiveView, NoteUpdateView, ParticipationDetailView, \
    PassageDetailView, PassageUpdateView, PoolCreateView, PoolDetailView, PoolJuryView, PoolNotesTemplateView, \
    PoolPresideJuryView, PoolRemoveJuryView, PoolUpdateView, PoolUploadNotesView, \
    ScaleNotationSheetTemplateView, SelectTeamFinalView, \
    SolutionsDownloadView, SolutionUploadView, \
    TeamAuthorizationsView, TeamDetailView, TeamLeaveView, TeamListView, TeamUpdateView, \
    TeamUploadMotivationLetterView, TournamentCreateView, TournamentDetailView, TournamentExportCSVView, \
    TournamentHarmonizeNoteView, TournamentHarmonizeView, TournamentListView, TournamentPaymentsView, \
    TournamentPublishNotesView, TournamentUpdateView, WrittenReviewUploadView


app_name = "participation"

urlpatterns = [
    path("create_team/", CreateTeamView.as_view(), name="create_team"),
    path("join_team/", JoinTeamView.as_view(), name="join_team"),
    path("teams/", TeamListView.as_view(), name="team_list"),
    path("team/", MyTeamDetailView.as_view(), name="my_team_detail"),
    path("team/<int:pk>/", TeamDetailView.as_view(), name="team_detail"),
    path("team/<int:pk>/update/", TeamUpdateView.as_view(), name="update_team"),
    path("team/<int:pk>/upload-motivation-letter/", TeamUploadMotivationLetterView.as_view(),
         name="upload_team_motivation_letter"),
    path("team/<int:team_id>/authorizations/", TeamAuthorizationsView.as_view(), name="team_authorizations"),
    path("team/leave/", TeamLeaveView.as_view(), name="team_leave"),
    path("detail/", MyParticipationDetailView.as_view(), name="my_participation_detail"),
    path("detail/<int:pk>/", ParticipationDetailView.as_view(), name="participation_detail"),
    path("detail/<int:pk>/solution/", SolutionUploadView.as_view(), name="upload_solution"),
    path("detail/<int:team_id>/solutions/", SolutionsDownloadView.as_view(), name="participation_solutions"),
    path("tournament/", TournamentListView.as_view(), name="tournament_list"),
    path("tournament/create/", TournamentCreateView.as_view(), name="tournament_create"),
    path("tournament/<int:pk>/", TournamentDetailView.as_view(), name="tournament_detail"),
    path("tournament/<int:pk>/update/", TournamentUpdateView.as_view(), name="tournament_update"),
    path("tournament/<int:pk>/payments/", TournamentPaymentsView.as_view(), name="tournament_payments"),
    path("tournament/<int:pk>/csv/", TournamentExportCSVView.as_view(), name="tournament_csv"),
    path("tournament/<int:tournament_id>/authorizations/", TeamAuthorizationsView.as_view(),
         name="tournament_authorizations"),
    path("tournament/<int:tournament_id>/solutions/", SolutionsDownloadView.as_view(),
         name="tournament_solutions"),
    path("tournament/<int:tournament_id>/written_reviews/", SolutionsDownloadView.as_view(),
         name="tournament_written_reviews"),
    path("tournament/<int:tournament_id>/notation/sheets/", NotationSheetsArchiveView.as_view(),
         name="tournament_notation_sheets"),
    path("tournament/<int:pk>/notation/notifications/", GSheetNotificationsView.as_view(),
         name="tournament_gsheet_notifications"),
    path("tournament/<int:pk>/publish-notes/<int:round>/", TournamentPublishNotesView.as_view(),
         name="tournament_publish_notes"),
    path("tournament/<int:pk>/harmonize/<int:round>/", TournamentHarmonizeView.as_view(),
         name="tournament_harmonize"),
    path("tournament/<int:pk>/harmonize/<int:round>/<str:action>/<str:trigram>/", TournamentHarmonizeNoteView.as_view(),
         name="tournament_harmonize_note"),
    path("tournament/<int:pk>/select-final/<int:participation_id>/", SelectTeamFinalView.as_view(),
         name="select_team_final"),
    path("pools/create/", PoolCreateView.as_view(), name="pool_create"),
    path("pools/<int:pk>/", PoolDetailView.as_view(), name="pool_detail"),
    path("pools/<int:pk>/update/", PoolUpdateView.as_view(), name="pool_update"),
    path("pools/<int:pool_id>/solutions/", SolutionsDownloadView.as_view(), name="pool_download_solutions"),
    path("pools/<int:pool_id>/written_reviews/", SolutionsDownloadView.as_view(), name="pool_download_written_reviews"),
    path("pools/<int:pk>/notation/scale/", ScaleNotationSheetTemplateView.as_view(), name="pool_scale_note_sheet"),
    path("pools/<int:pk>/notation/final/", FinalNotationSheetTemplateView.as_view(), name="pool_final_note_sheet"),
    path("pools/<int:pool_id>/notation/sheets/", NotationSheetsArchiveView.as_view(), name="pool_notation_sheets"),
    path("pools/<int:pk>/jury/", PoolJuryView.as_view(), name="pool_jury"),
    path("pools/<int:pk>/jury/remove/<int:jury_id>/", PoolRemoveJuryView.as_view(), name="pool_remove_jury"),
    path("pools/<int:pk>/jury/preside/<int:jury_id>/", PoolPresideJuryView.as_view(), name="pool_preside"),
    path("pools/<int:pk>/upload-notes/", PoolUploadNotesView.as_view(), name="pool_upload_notes"),
    path("pools/<int:pk>/upload-notes/template/", PoolNotesTemplateView.as_view(), name="pool_notes_template"),
    path("pools/passages/<int:pk>/", PassageDetailView.as_view(), name="passage_detail"),
    path("pools/passages/<int:pk>/update/", PassageUpdateView.as_view(), name="passage_update"),
    path("pools/passages/<int:pk>/written_review/", WrittenReviewUploadView.as_view(), name="upload_written_review"),
    path("pools/passages/notes/<int:pk>/", NoteUpdateView.as_view(), name="update_notes"),
]
