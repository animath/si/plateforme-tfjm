# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from rest_framework import serializers

from ..models import Note, Participation, Passage, Pool, Solution, Team, Tournament, WrittenReview


class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = '__all__'


class ParticipationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participation
        fields = '__all__'


class PassageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passage
        fields = '__all__'


class PoolSerializer(serializers.ModelSerializer):
    passages = serializers.ListSerializer(child=PassageSerializer(), read_only=True)

    class Meta:
        model = Pool
        fields = '__all__'


class SolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solution
        fields = '__all__'


class WrittenReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = WrittenReview
        fields = '__all__'


class TeamSerializer(serializers.ModelSerializer):
    participation = ParticipationSerializer()

    class Meta:
        model = Team
        fields = '__all__'


class TournamentSerializer(serializers.ModelSerializer):
    participations = serializers.ListSerializer(child=ParticipationSerializer())

    class Meta:
        model = Tournament
        fields = ('id', 'pk', 'name', 'date_start', 'date_end', 'place', 'max_teams', 'price', 'remote',
                  'inscription_limit', 'solution_limit', 'solutions_draw', 'reviews_first_phase_limit',
                  'solutions_available_second_phase', 'reviews_second_phase_limit',
                  'solutions_available_third_phase', 'reviews_third_phase_limit',
                  'description', 'organizers', 'final', 'participations',)


class TweakSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'
