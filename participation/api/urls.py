# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from .views import NoteViewSet, ParticipationViewSet, PassageViewSet, PoolViewSet, \
    SolutionViewSet, TeamViewSet, TournamentViewSet, TweakViewSet, WrittenReviewViewSet


def register_participation_urls(router, path):
    """
    Configure router for participation REST API.
    """
    router.register(path + "/note", NoteViewSet)
    router.register(path + "/participation", ParticipationViewSet)
    router.register(path + "/passage", PassageViewSet)
    router.register(path + "/pool", PoolViewSet)
    router.register(path + "/review", WrittenReviewViewSet)
    router.register(path + "/solution", SolutionViewSet)
    router.register(path + "/team", TeamViewSet)
    router.register(path + "/tournament", TournamentViewSet)
    router.register(path + "/tweak", TweakViewSet)
