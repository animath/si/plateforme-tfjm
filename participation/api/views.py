# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.viewsets import ModelViewSet

from .serializers import NoteSerializer, ParticipationSerializer, PassageSerializer, PoolSerializer, \
    SolutionSerializer, TeamSerializer, TournamentSerializer, TweakSerializer, WrittenReviewSerializer
from ..models import Note, Participation, Passage, Pool, Solution, Team, Tournament, Tweak, WrittenReview


class NoteViewSet(ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['jury', 'passage', 'reporter_writing', 'reporter_oral', 'opponent_writing',
                        'opponent_oral', 'reviewer_writing', 'reviewer_oral', 'observer_writing', 'observer_oral', ]


class ParticipationViewSet(ModelViewSet):
    queryset = Participation.objects.all()
    serializer_class = ParticipationSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['team', 'team__name', 'team__trigram', 'tournament', 'tournament__name', 'valid', 'final', ]


class PassageViewSet(ModelViewSet):
    queryset = Passage.objects.all()
    serializer_class = PassageSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pool', 'solution_number', 'reporter', 'opponent', 'reviewer', 'observer', 'pool_tournament', ]


class PoolViewSet(ModelViewSet):
    queryset = Pool.objects.all()
    serializer_class = PoolSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['tournament', 'tournament__name', 'round', 'participations', 'juries', 'bbb_url', ]


class SolutionViewSet(ModelViewSet):
    queryset = Solution.objects.all()
    serializer_class = SolutionSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['participation', 'number', 'problem', 'final_solution', ]


class WrittenReviewViewSet(ModelViewSet):
    queryset = WrittenReview.objects.all()
    serializer_class = WrittenReviewSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['participation', 'number', 'passage', 'type', ]


class TeamViewSet(ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'trigram', 'access_code', 'participation__valid', 'participation__tournament',
                        'participation__tournament__name', 'participation__valid', 'participation__final', ]


class TournamentViewSet(ModelViewSet):
    queryset = Tournament.objects.all()
    serializer_class = TournamentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'date_start', 'date_end', 'place', 'max_teams', 'price', 'remote',
                        'inscription_limit', 'solution_limit', 'solutions_draw', 'reviews_first_phase_limit',
                        'solutions_available_second_phase', 'reviews_second_phase_limit',
                        'solutions_available_third_phase', 'reviews_third_phase_limit',
                        'description', 'organizers', 'final', ]


class TweakViewSet(ModelViewSet):
    queryset = Tweak.objects.all()
    serializer_class = TweakSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pool', 'pool__tournament', 'pool__tournament__name', 'participation',
                        'participation__team__trigram', 'diff', ]
