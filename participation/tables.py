# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.utils import formats
from django.utils.safestring import mark_safe
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
import django_tables2 as tables

from .models import Note, Passage, Pool, Team, Tournament


# noinspection PyTypeChecker
class TeamTable(tables.Table):
    name = tables.LinkColumn(
        'participation:team_detail',
        args=[tables.A("id")],
        verbose_name=lambda: _("name").capitalize(),
    )

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped',
        }
        model = Team
        fields = ('name', 'trigram',)


# noinspection PyTypeChecker
class ParticipationTable(tables.Table):
    name = tables.LinkColumn(
        'participation:team_detail',
        args=[tables.A("team__id")],
        verbose_name=_("name").capitalize,
        accessor="team__name",
    )

    trigram = tables.Column(
        verbose_name=_("trigram").capitalize,
        accessor="team__trigram",
    )

    valid = tables.Column(
        verbose_name=_("valid").capitalize,
        accessor="valid",
        empty_values=(),
    )

    def render_valid(self, value):
        return _("Validated") if value else _("Validation pending") if value is False else _("Not validated")

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped',
        }
        model = Team
        fields = ('name', 'trigram', 'valid',)
        order = ('-valid',)


class TournamentTable(tables.Table):
    name = tables.LinkColumn()

    date = tables.Column(_("date").capitalize, accessor="id")

    def render_date(self, record):
        return format_lazy(_("From {start} to {end}"),
                           start=formats.date_format(record.date_start, format="SHORT_DATE_FORMAT", use_l10n=True),
                           end=formats.date_format(record.date_end, format="SHORT_DATE_FORMAT", use_l10n=True))

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped',
        }
        model = Tournament
        fields = ('name', 'date',)
        order_by = ('name', )


class PoolTable(tables.Table):
    letter = tables.LinkColumn(
        'participation:pool_detail',
        args=[tables.A('id')],
        verbose_name=_("pool").capitalize,
    )

    teams = tables.Column(
        verbose_name=_("teams").capitalize,
        empty_values=(),
        orderable=False,
    )

    def render_letter(self, record):
        return format_lazy(_("Pool {code}"), code=record.short_name)

    def render_teams(self, record):
        return ", ".join(participation.team.trigram for participation in record.participations.all()) \
               or _("No defined team")

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped',
        }
        model = Pool
        fields = ('letter', 'teams', 'round', 'tournament',)


class PassageTable(tables.Table):
    # FIXME Ne pas afficher l'équipe observatrice si non nécessaire

    reporter = tables.LinkColumn(
        "participation:passage_detail",
        args=[tables.A("id")],
        verbose_name=_("reporter").capitalize,
    )

    def render_reporter(self, value):
        return value.team.trigram

    def render_opponent(self, value):
        return value.team.trigram

    def render_reviewer(self, value):
        return value.team.trigram

    def render_observer(self, value):
        return value.team.trigram

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped text-center',
        }
        model = Passage
        fields = ('reporter', 'opponent', 'reviewer', 'observer', 'solution_number', )


class NoteTable(tables.Table):
    jury = tables.Column(
        attrs={
            "td": {
                "class": "text-nowrap",
            }
        }
    )

    update = tables.Column(
        verbose_name=_("Update"),
        accessor="id",
        empty_values=(),
    )

    def render_update(self, record):
        return mark_safe(f'<button class="btn btn-info" data-bs-toggle="modal" '
                         f'data-bs-target="#{record.modal_name}Modal">'
                         f'{_("Update")}</button>')

    class Meta:
        attrs = {
            'class': 'table table-condensed table-striped text-center',
        }
        model = Note
        fields = ('jury', 'reporter_writing', 'reporter_oral', 'opponent_writing', 'opponent_oral',
                  'reviewer_writing', 'reviewer_oral', 'observer_writing', 'observer_oral', 'update',)
