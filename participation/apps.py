# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.db.models.signals import post_save, pre_save


class ParticipationConfig(AppConfig):
    """
    The participation app contains the data about the teams, solutions, ...
    """
    name = 'participation'

    def ready(self):
        from participation import signals
        pre_save.connect(signals.update_mailing_list, "participation.Team")
        post_save.connect(signals.create_team_participation, "participation.Team")
        post_save.connect(signals.create_payments, "participation.Participation")
        post_save.connect(signals.create_notes, "participation.Passage")
        post_save.connect(signals.create_notes, "participation.Pool")
