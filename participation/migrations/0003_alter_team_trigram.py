# Generated by Django 3.2.18 on 2023-02-19 22:13

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('participation', '0002_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='trigram',
            field=models.CharField(help_text='The trigram must be composed of three uppercase letters.', max_length=3, unique=True, validators=[django.core.validators.RegexValidator('^[A-Z]{3}$'), django.core.validators.RegexValidator('^(?!BIT$|CNO$|CRO$|CUL$|FTG$|FCK$|FUC$|FUK$|FYS$|HIV$|IST$|MST$|KKK$|KYS$|SEX$)', message='This trigram is forbidden.')], verbose_name='trigram'),
        ),
    ]
