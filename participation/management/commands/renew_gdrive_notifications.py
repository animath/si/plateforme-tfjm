# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from hashlib import sha1

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management import BaseCommand
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import localtime
import gspread

from ...models import Tournament


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--tournament', '-t', help="Tournament name to update (if not set, all tournaments will be updated)",
        )

    def handle(self, *args, **options):
        tournaments = Tournament.objects.all() if not options['tournament'] \
            else Tournament.objects.filter(name=options['tournament']).all()

        gc = gspread.service_account_from_dict(settings.GOOGLE_SERVICE_CLIENT)
        http_client = gc.http_client
        http_client.login()

        site = Site.objects.get(pk=settings.SITE_ID)

        now = localtime(timezone.now())
        tomorrow = now + timezone.timedelta(days=1)
        tomorrow -= timezone.timedelta(hours=now.hour, minutes=now.minute, seconds=now.second,
                                       microseconds=now.microsecond)

        for tournament in tournaments:
            if options['verbosity'] >= 1:
                self.stdout.write(f"Renewing Google Drive notifications for {tournament}")

            if not tournament.notes_sheet_id:
                if options['verbosity'] >= 1:
                    self.stdout.write(
                        self.style.WARNING(f"No spreadsheet found for {tournament}. Please create it first"))
                continue

            channel_id = sha1(f"{tournament.name}-{now.date()}-{site.domain}".encode()).hexdigest()
            url = f"https://www.googleapis.com/drive/v3/files/{tournament.notes_sheet_id}/watch?supportsAllDrives=true"
            notif_path = reverse('participation:tournament_gsheet_notifications', args=[tournament.pk])
            notif_url = f"https://{site.domain}{notif_path}"
            body = {
                "id": channel_id,
                "type": "web_hook",
                "address": notif_url,
                "expiration": str(int(1000 * tomorrow.timestamp())),
            }
            try:
                http_client.request(method="POST", endpoint=url, json=body).raise_for_status()
            except Exception as e:
                self.stderr.write(self.style.ERROR(f"Error while renewing notifications for {tournament.name}: {e}"))
