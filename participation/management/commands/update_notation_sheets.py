# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.management import BaseCommand
from participation.models import Tournament


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--tournament', '-t', help="Tournament name to update (if not set, all tournaments will be updated)",
        )
        parser.add_argument(
            '--round', '-r', type=int, help="Round number to update (if not set, all rounds will be updated)",
        )
        parser.add_argument(
            '--letter', '-l', help="Letter of the pool to update (if not set, all pools will be updated)",
        )

    def handle(self, *args, **options):
        tournaments = Tournament.objects.all() if not options['tournament'] \
            else Tournament.objects.filter(name=options['tournament']).all()

        for tournament in tournaments:
            if options['verbosity'] >= 1:
                self.stdout.write(f"Updating notation sheet for {tournament}")
            tournament.create_spreadsheet()

            pools = tournament.pools.all()
            if options['round']:
                pools = pools.filter(round=options['round'])
            if options['letter']:
                pools = pools.filter(letter=ord(options['letter']) - 64)
            for pool in pools.all():
                if options['verbosity'] >= 1:
                    self.stdout.write(f"Updating notation sheet for pool {pool.short_name} for {tournament}")
                pool.update_spreadsheet()

            tournament.update_ranking_spreadsheet()
