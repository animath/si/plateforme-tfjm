# Copyright (C) 2021 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.core.management import BaseCommand
from django.utils.formats import date_format
from django.utils.translation import activate
from participation.models import Tournament


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        activate(settings.PREFERRED_LANGUAGE_CODE)

        tournaments = Tournament.objects.order_by('-date_start', 'name')
        for tournament in tournaments:
            self.handle_tournament(tournament)
            self.w("")
            self.w("")

    def w(self, msg, prefix="", suffix=""):
        self.stdout.write(f"{prefix}{msg}{suffix}")

    def handle_tournament(self, tournament):
        name = tournament.name
        date_start = date_format(tournament.date_start, "DATE_FORMAT")
        date_end = date_format(tournament.date_end, "DATE_FORMAT")

        notes = dict()
        for participation in tournament.participations.filter(valid=True).all():
            note = sum(pool.average(participation)
                       for pool in tournament.pools.filter(participations=participation).all())
            notes[participation] = note
        notes = sorted(notes.items(), key=lambda x: x[1], reverse=True)

        self.w("<!-- wp:heading {\"level\":3} -->")
        self.w(f"<h3><strong>{name}</strong></h3>")
        self.w("<!-- /wp:heading -->")
        self.w("")
        self.w("<!-- wp:paragraph -->")
        if tournament.final:
            self.w(f"<p>La finale a eu lieu le weekend du {date_start} au {date_end} et a été remporté par l'équipe "
                   f"<em>{notes[0][0].team.name}</em> suivie de l'équipe <em>{notes[1][0].team.name}</em>. "
                   f"Les deux premières équipes sont sélectionnées pour représenter la France lors de l'ETEAM.</p>")
        else:
            self.w(f"<p>Le tournoi de {name} a eu lieu le weekend du {date_start} au {date_end} et a été remporté par "
                   f"l'équipe <em>{notes[0][0].team.name}</em>.</p>")
        self.w("<!-- /wp:paragraph -->")
        self.w("")
        self.w("")
        self.w("<!-- wp:table -->")
        self.w("<figure class=\"wp-block-table\">")
        self.w("<table>")
        self.w("<thead>")
        self.w("<tr>")
        self.w("    <th>Équipe</th>")
        self.w("    <th>Score Tour 1</th>")
        self.w("    <th>Score Tour 2</th>")
        self.w("    <th>Total</th>")
        self.w("    <th class=\"has-text-align-center\">Prix</th>")
        self.w("</tr>")
        self.w("</thead>")
        self.w("<tbody>")
        for i, (participation, note) in enumerate(notes):
            self.w("<tr>")
            bold = (not tournament.final and participation.final) or (tournament.final and i < 2)
            if bold:
                prefix, suffix = "    <td><strong>", "</strong></td>"
            else:
                prefix, suffix = "    <td>", "</td>"
            self.w(f"{participation.team.name} ({participation.team.trigram})", prefix, suffix)
            for tournament_round in [1, 2]:
                pool_note = sum(pool.average(participation)
                                for pool in tournament.pools.filter(participations=participation,
                                                                    round=tournament_round).all())
                self.w(f"{pool_note:.01f}", prefix, suffix)
            self.w(f"{note:.01f}", prefix, suffix)
            self.w(participation.mention_final if tournament.final else participation.mention, prefix, suffix)
            self.w("</tr>")
        self.w("</tbody>")
        self.w("</table>")
        self.w("</figure>")
        self.w("<!-- /wp:table -->")
