# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class APIConfig(AppConfig):
    """
    Manage the inscription through a JSON API.
    """
    name = 'api'
    verbose_name = _('API')
