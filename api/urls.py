# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.urls import include, path
from rest_framework import routers

from .viewsets import UserViewSet

# Routers provide an easy way of automatically determining the URL conf.
# Register each app API router and user viewset
router = routers.DefaultRouter()
router.register('user', UserViewSet)

if "logs" in settings.INSTALLED_APPS:
    from logs.api.urls import register_logs_urls
    register_logs_urls(router, "logs")

if "participation" in settings.INSTALLED_APPS:
    from participation.api.urls import register_participation_urls
    register_participation_urls(router, "participation")

if "registration" in settings.INSTALLED_APPS:
    from registration.api.urls import register_registration_urls
    register_registration_urls(router, "registration")

app_name = 'api'

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
