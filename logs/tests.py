from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.test import TestCase

from .models import Changelog


class TestChangelog(TestCase):
    def test_logs(self):
        user = User.objects.create(email="admin@example.com")
        self.assertTrue(Changelog.objects.filter(action="create", instance_pk=user.pk,
                                                 model=ContentType.objects.get_for_model(User)).exists())
        old_user_pk = user.pk
        user.delete()
        self.assertTrue(Changelog.objects.filter(action="delete", instance_pk=old_user_pk,
                                                 model=ContentType.objects.get_for_model(User)).exists())

        changelog = Changelog.objects.first()
        self.assertRaises(ValidationError, changelog.delete)
        str(Changelog.objects.all())
