FROM python:3.13-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ALLOW_ASYNC_UNSAFE 1

RUN apk add --no-cache gettext nginx gcc git libc-dev libffi-dev libpq-dev libxml2-dev libxslt-dev \
    npm libmagic texlive texmf-dist-fontsrecommended texmf-dist-lang texmf-dist-latexextra

RUN apk add --no-cache bash

RUN npm install -g yuglify

RUN mkdir /code /code/docs
WORKDIR /code
COPY requirements.txt /code/requirements.txt
COPY docs/requirements.txt /code/docs/requirements.txt
RUN pip install -r requirements.txt --no-cache-dir
RUN pip install -r docs/requirements.txt --no-cache-dir

COPY . /code/

# Compile documentation
RUN sphinx-build -M html docs docs/_build

RUN python manage.py collectstatic --noinput && \
        python manage.py compilemessages

# Configure nginx
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /code/nginx_tfjm.conf /etc/nginx/http.d/tfjm.conf && rm /etc/nginx/http.d/default.conf

RUN crontab /code/tfjm.cron

# With a bashrc, the shell is better
RUN ln -s /code/.bashrc /root/.bashrc

ENTRYPOINT ["/code/entrypoint.sh"]
EXPOSE 80

CMD ["./manage.py", "shell_plus", "--ipython"]
