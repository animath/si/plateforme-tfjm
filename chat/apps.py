# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.db.models.signals import post_save


class ChatConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "chat"

    def ready(self):
        from chat import signals
        post_save.connect(signals.create_tournament_channels, "participation.Tournament")
        post_save.connect(signals.create_pool_channels, "participation.Pool")
        post_save.connect(signals.create_team_channel, "participation.Participation")
