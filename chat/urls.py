# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from django.utils.translation import gettext_lazy as _
from tfjm.views import LoginRequiredTemplateView

app_name = 'chat'

urlpatterns = [
    path('', LoginRequiredTemplateView.as_view(template_name="chat/chat.html",
                                               extra_context={'title': _("Chat")}), name='chat'),
    path('fullscreen/', LoginRequiredTemplateView.as_view(template_name="chat/fullscreen.html", login_url='chat:login'),
         name='fullscreen'),
    path('login/', LoginView.as_view(template_name="chat/login.html"), name='login'),
    path('logout/', LogoutView.as_view(next_page='chat:fullscreen'), name='logout'),
]
