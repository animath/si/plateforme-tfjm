# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from chat.models import Channel
from participation.models import Participation, Pool, Tournament
from tfjm.permissions import PermissionType


def create_tournament_channels(instance: Tournament, **_kwargs):
    """
    Lorsqu'un tournoi est créé, on crée les canaux de chat associés.
    On crée notamment un canal d'annonces (accessible en écriture uniquement aux orgas),
    un canal général, un de détente, un pour les juré⋅es et un pour les président⋅es de jury.
    """
    tournament = instance

    # Création du canal « Tournoi - Annonces »
    Channel.objects.update_or_create(
        name=f"{tournament.name} - Annonces",
        defaults=dict(
            category=Channel.ChannelCategory.TOURNAMENT,
            read_access=PermissionType.TOURNAMENT_MEMBER,
            write_access=PermissionType.TOURNAMENT_ORGANIZER,
            tournament=tournament,
        ),
    )

    # Création du canal « Tournoi - Général »
    Channel.objects.update_or_create(
        name=f"{tournament.name} - Général",
        defaults=dict(
            category=Channel.ChannelCategory.TOURNAMENT,
            read_access=PermissionType.TOURNAMENT_MEMBER,
            write_access=PermissionType.TOURNAMENT_MEMBER,
            tournament=tournament,
        ),
    )

    # Création du canal « Tournoi - Détente »
    Channel.objects.update_or_create(
        name=f"{tournament.name} - Détente",
        defaults=dict(
            category=Channel.ChannelCategory.TOURNAMENT,
            read_access=PermissionType.TOURNAMENT_MEMBER,
            write_access=PermissionType.TOURNAMENT_MEMBER,
            tournament=tournament,
        ),
    )

    # Création du canal « Tournoi - Juré⋅es »
    Channel.objects.update_or_create(
        name=f"{tournament.name} - Juré⋅es",
        defaults=dict(
            category=Channel.ChannelCategory.TOURNAMENT,
            read_access=PermissionType.JURY_MEMBER,
            write_access=PermissionType.JURY_MEMBER,
            tournament=tournament,
        ),
    )

    if tournament.remote:
        # Création du canal « Tournoi - Président⋅es de jury » dans le cas d'un tournoi distanciel
        Channel.objects.update_or_create(
            name=f"{tournament.name} - Président⋅es de jury",
            defaults=dict(
                category=Channel.ChannelCategory.TOURNAMENT,
                read_access=PermissionType.TOURNAMENT_JURY_PRESIDENT,
                write_access=PermissionType.TOURNAMENT_JURY_PRESIDENT,
                tournament=tournament,
            ),
        )


def create_pool_channels(instance: Pool, **_kwargs):
    """
    Lorsqu'une poule est créée, on crée les canaux de chat associés.
    On crée notamment un canal pour les membres de la poule et un pour les juré⋅es.
    Cela ne concerne que les tournois distanciels.
    """
    pool = instance
    tournament = pool.tournament

    if tournament.remote:
        # Dans le cadre d'un tournoi distanciel, on crée un canal pour les membres de la poule
        # et un pour les juré⋅es de la poule.
        Channel.objects.update_or_create(
            name=f"{tournament.name} - Poule {pool.short_name}",
            defaults=dict(
                category=Channel.ChannelCategory.TOURNAMENT,
                read_access=PermissionType.POOL_MEMBER,
                write_access=PermissionType.POOL_MEMBER,
                pool=pool,
            ),
        )

        Channel.objects.update_or_create(
            name=f"{tournament.name} - Poule {pool.short_name} - Jury",
            defaults=dict(
                category=Channel.ChannelCategory.TOURNAMENT,
                read_access=PermissionType.JURY_MEMBER,
                write_access=PermissionType.JURY_MEMBER,
                pool=pool,
            ),
        )


def create_team_channel(instance: Participation, **_kwargs):
    """
    Lorsqu'une équipe est validée, on crée un canal de chat associé.
    """
    if instance.valid:
        Channel.objects.update_or_create(
            name=f"Équipe {instance.team.trigram}",
            defaults=dict(
                category=Channel.ChannelCategory.TEAM,
                read_access=PermissionType.TEAM_MEMBER,
                write_access=PermissionType.TEAM_MEMBER,
                team=instance.team,
            ),
        )
