# Copyright (C) 2024 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin

from .models import Channel, Message


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    """
    Modèle d'administration des canaux de chat.
    """
    list_display = ('name', 'category', 'read_access', 'write_access', 'tournament', 'private',)
    list_filter = ('category', 'read_access', 'write_access', 'tournament', 'private',)
    search_fields = ('name', 'tournament__name', 'team__name', 'team__trigram',)
    autocomplete_fields = ('tournament', 'pool', 'team', 'invited', )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    """
    Modèle d'administration des messages de chat.
    """
    list_display = ('channel', 'author', 'created_at', 'updated_at', 'content',)
    list_filter = ('channel', 'created_at', 'updated_at',)
    search_fields = ('author__username', 'author__first_name', 'author__last_name', 'content',)
    autocomplete_fields = ('channel', 'author', 'users_read',)
