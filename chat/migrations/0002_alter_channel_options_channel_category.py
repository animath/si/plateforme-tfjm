# Generated by Django 5.0.3 on 2024-04-28 11:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("chat", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="channel",
            options={
                "ordering": ("category", "name"),
                "verbose_name": "channel",
                "verbose_name_plural": "channels",
            },
        ),
        migrations.AddField(
            model_name="channel",
            name="category",
            field=models.CharField(
                choices=[
                    ("general", "General channels"),
                    ("tournament", "Tournament channels"),
                    ("team", "Team channels"),
                    ("private", "Private channels"),
                ],
                default="general",
                max_length=255,
                verbose_name="category",
            ),
        ),
    ]
